namespace VetApp.API.Dtos
{
    public class VacinaToConsultaDto
    {
        public int Id { get; set; }
        public string NomeVacina { get; set; }

        public double QuantidadeAplicada { get; set; }
    }
}