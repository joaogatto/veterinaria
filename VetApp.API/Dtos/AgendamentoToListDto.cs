using System;
using System.ComponentModel.DataAnnotations;

namespace VetApp.API.Dtos
{
    public class AgendamentoToListDto
    {
        public int Id { get; set; }
        public KeyValuePair Animal { get; set; }
        public KeyValuePair Cliente { get; set; }
        public int TipoAgendamento { get; set; }
        public DateTime DataHora { get; set; }
        public KeyValuePair Funcionario { get; set; }
        public int StatusId { get; set; }
        [MaxLength]
        public string Observacoes { get; set; }
    }
}