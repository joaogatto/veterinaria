namespace VetApp.API.Dtos
{
    public class ExameFinalizarDto
    {
        public int? Status { get; set; }
        public string Arquivo { get; set; }
        public int? FuncionarioId { get; set; }
        public string Resultado { get; set; }
    }
}