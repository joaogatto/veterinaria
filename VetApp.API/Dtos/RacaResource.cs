using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace VetApp.API.Dtos
{
    public class RacaResource
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public KeyValuePair Especie { get; set; }
        public ICollection<KeyValuePair> Animais { get; set; }

        public RacaResource()
        {
            this.Animais = new Collection<KeyValuePair>();
        }
    }
}