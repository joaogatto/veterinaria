namespace VetApp.API.Dtos
{
    public class CancelarAgendamentoDto
    {
        public int AgendamentoId { get; set; }
        public string Motivo { get; set; }
    }
}