using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VetApp.API.Dtos
{
    public class ConsultaFromCadastroDto
    {
        public int AgendamentoId { get; set; }
        public int AnimalId { get; set; }
        public DateTime DataHoraConsulta { get; set; }
        [MaxLength]
        public string QueixaInicial { get; set; }
        public double? Temperatura { get; set; }
        public double? Peso { get; set; }
        [MaxLength]
        public string ExameClinico { get; set; }
        [MaxLength]
        public string Diagnostico { get; set; }
        public ICollection<SintomaDto> Sintomas { get; set; }
        public ICollection<KeyValuePair> Vacinas { get; set; }
        public ICollection<KeyValuePair> Exames { get; set; }
        public ICollection<MedicamentosFromConsultaDto> Medicamentos { get; set; }
    }
}