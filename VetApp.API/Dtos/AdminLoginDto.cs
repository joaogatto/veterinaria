namespace VetApp.API.Dtos
{
    public class AdminLoginDto
    {
        public string Email { get; set; }
        public string Senha { get; set; }
    }
}