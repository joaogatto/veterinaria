using System;
using System.ComponentModel.DataAnnotations;

namespace VetApp.API.Dtos
{
    public class AgendamentoFromClinicaDto
    {
        public int AnimalId { get; set; }
        public int FuncionarioId { get; set; }
        public int ClinicaId { get; set; }
        public int ClienteId { get; set; }
        public int TipoAgendamento { get; set; }
        public double DataHora { get; set; }
        [MaxLength]
        public string Observacoes { get; set; }
    }
}