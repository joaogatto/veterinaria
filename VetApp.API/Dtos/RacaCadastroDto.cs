namespace VetApp.API.Dtos
{
    public class RacaCadastroDto
    {
        public string Nome { get; set; }
        public int EspecieId { get; set; }
    }
}