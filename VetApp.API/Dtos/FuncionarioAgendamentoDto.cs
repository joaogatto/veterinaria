namespace VetApp.API.Dtos
{
    public class FuncionarioAgendamentoDto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Role { get; set; }
    }
}