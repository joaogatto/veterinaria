using System.ComponentModel.DataAnnotations;

namespace VetApp.API.Dtos
{
    public class ClinicaCadastroDto
    {
        [Required]
        public string ClinicaNome { get; set; }
        
        [Required]
        public string ClinicaLogin { get; set; }
        
        [Required]
        public string Responsavel { get; set; }
        
        [Required]
        public string Endereco { get; set; }
        
        [Required]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "A senha deve ter entre 4 e 20 caracteres")]
        public string Senha { get; set; }

        [Required]
        public FuncionarioCadastroDto Funcionario { get; set; }
    }
}