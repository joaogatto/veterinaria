using System;

namespace VetApp.API.Dtos
{
    public class AgendamentosDoFuncionarioDto
    {
        public DateTime DataHora { get; set; }

        public int TipoAgendamento { get; set; }

        public KeyValuePair Status { get; set; }
    }
}