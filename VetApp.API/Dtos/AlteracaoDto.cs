namespace VetApp.API.Dtos
{
    public class AlteracaoDto
    {
        public int QuemAlterouId { get; set; }
        public string QuemAlterouRole { get; set; }
    }
}