namespace VetApp.API.Dtos
{
    public class ExameFromClinicaDto
    {
        public string Nome { get; set; }
        public int AnimalId { get; set; }
        public int? ConsultaId { get; set; }
        public int FuncionarioId { get; set; }
        public string Resultado { get; set; }
        public int Status { get; set; }
    }
}