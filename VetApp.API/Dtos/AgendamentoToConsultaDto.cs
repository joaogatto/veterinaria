using System;

namespace VetApp.API.Dtos
{
    public class AgendamentoToConsultaDto
    {
        public int Id { get; set; }
        public KeyValuePair Cliente { get; set; }
        public KeyValuePair Animal { get; set; }
        public KeyValuePair RacaAnimal { get; set; }
        public DateTime DataNascAnimal { get; set; }

        public string Observacoes { get; set; }
    }
}