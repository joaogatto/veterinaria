using System;

namespace VetApp.API.Dtos
{
    public class AniVacToVinculoDto
    {
        public int Id { get; set; }
        public KeyValuePair Animal { get; set; }
        
        public KeyValuePair Vacina { get; set; }
        public DateTime DataHoraAplicacao { get; set; }
        public double QuantidadeAplicada { get; set; }
    }
}