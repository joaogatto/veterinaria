using System;

namespace VetApp.API.Dtos
{
    public class VacinaAplicacaoDto
    {
        public int AnimalId { get; set; }
        public int VacinaId { get; set; }
        public int FuncionarioId { get; set; }
        public double QuantidadeAplicada { get; set; }
        public DateTime DataHoraAplicacao { get; set; }
    }
}