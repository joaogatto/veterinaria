using System;

namespace VetApp.API.Dtos
{
    public class AnimalCadastroDto
    {
        public string Nome { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Cor { get; set; }
        public int ClienteId { get; set; }
        public int RacaId { get; set; }
    }
}