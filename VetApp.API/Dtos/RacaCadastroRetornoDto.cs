namespace VetApp.API.Dtos
{
    public class RacaCadastroRetornoDto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int EspecieId { get; set; }
    }
}