using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace VetApp.API.Dtos
{
    public class EspecieResource
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public ICollection<KeyValuePair> Racas { get; set; }

        public EspecieResource()
        {
            this.Racas = new Collection<KeyValuePair>();
        }
    }
}