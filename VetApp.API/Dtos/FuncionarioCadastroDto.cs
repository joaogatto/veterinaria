using System.ComponentModel.DataAnnotations;

namespace VetApp.API.Dtos
{
    public class FuncionarioCadastroDto
    {
        public int ClinicaId { get; set; }

        [Required]
        [MinLength(3)]
        public string Nome { get; set; }
        
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        
        [Required]
        [StringLength(20, MinimumLength = 4, ErrorMessage = "A senha deve ter entre 4 e 20 caracteres")]
        public string Senha { get; set; }
        
        [Required]
        public string Role { get; set; }
    }
}