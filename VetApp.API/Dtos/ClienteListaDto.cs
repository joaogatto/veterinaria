using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace VetApp.API.Dtos
{
    public class ClienteListaDto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public string OutroNumero { get; set; }
        public string Email { get; set;}
        public ICollection<KeyValuePair> Animais { get; set;}

        public ClienteListaDto()
        {
            this.Animais = new Collection<KeyValuePair>();
        }
    }
}