namespace VetApp.API.Dtos
{
    public class MedicamentosFromConsultaDto
    {
        public string Nome { get; set; }
        public double Quantidade { get; set; }
        public string UnidadeDeMedida { get; set; }
        public string ComoSerTomado { get; set; }
        public int? ConsultaId { get; set; }
    }
}