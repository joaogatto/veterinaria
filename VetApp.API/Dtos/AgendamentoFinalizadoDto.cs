namespace VetApp.API.Dtos
{
    public class AgendamentoFinalizadoDto
    {
        public int Id { get; set; }
        public string Foto { get; set; }
    }
}