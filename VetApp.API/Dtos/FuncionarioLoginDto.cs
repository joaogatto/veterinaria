namespace VetApp.API.Dtos
{
    public class FuncionarioLoginDto
    {
        public string Clinica { get; set; }
        public string SenhaClinica { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
    }
}