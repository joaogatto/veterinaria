namespace VetApp.API.Dtos
{
    public class VincularClienteDto
    {
        public string Email { get; set; }
        public string Senha { get; set; }
        public int ClinicaId { get; set; }
    }
}