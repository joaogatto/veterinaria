using System;

namespace VetApp.API.Models
{
    public class AnimalPeso
    {
        public int Id { get; set; }
        public int AnimalId { get; set; }
        public Animal Animal { get; set; }
        public double Peso { get; set; }
        public DateTime Data { get; set; }
    }
}