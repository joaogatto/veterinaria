using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace VetApp.API.Models
{
    public class Vacina
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public ICollection<AnimalVacina> AnimaisVacina { get; set; }
        
        public Vacina()
        {
            this.AnimaisVacina = new Collection<AnimalVacina>();
        }
    }
}