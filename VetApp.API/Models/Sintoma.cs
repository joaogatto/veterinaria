using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace VetApp.API.Models
{
    public class Sintoma
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public ICollection<ConsultaSintoma> Consultas { get; set; }

        public Sintoma()
        {
            this.Consultas = new Collection<ConsultaSintoma>();
        }
    }
}