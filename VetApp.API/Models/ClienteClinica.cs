namespace VetApp.API.Models
{
    public class ClienteClinica
    {
        public int ClinicaId { get; set; }
        public Clinica Clinica { get; set; }
        public int ClienteId { get; set; }
        public Cliente Cliente { get; set; }
    }
}