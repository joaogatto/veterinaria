using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace VetApp.API.Models
{
    public class Agendamento
    {
        public int Id { get; set; }
        public int AnimalId { get; set; }
        public Animal Animal { get; set; }
        public int ClinicaId { get; set; }
        public Clinica Clinica { get; set; }
        public int ClienteId { get; set; }
        public Cliente Cliente { get; set; }
        public int TipoAgendamento { get; set; }
        public DateTime DataHora { get; set; }
        public int? FuncionarioId { get; set; }
        public string Foto { get; set; }
        public Funcionario Funcionario { get; set; }
        [MaxLength]
        public string Observacoes { get; set; }
        public ICollection<AgendamentoStatus> AgendamentoStatus { get; set; }

        public Agendamento()
        {
            this.AgendamentoStatus = new Collection<AgendamentoStatus>();
        }
    }
}