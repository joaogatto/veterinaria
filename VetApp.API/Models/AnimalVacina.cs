using System;

namespace VetApp.API.Models
{
    public class AnimalVacina
    {
        public int Id { get; set; }
        public int AnimalId { get; set; }
        public Animal Animal { get; set; }
        public int VacinaId { get; set; }
        
        public Vacina Vacina { get; set; }
        public int FuncionarioId { get; set; }
        public Funcionario Funcionario { get; set; }
        public int? ConsultaId { get; set; }
        public Consulta Consulta { get; set; }
        public DateTime DataHoraAplicacao { get; set; }
        public double QuantidadeAplicada { get; set; }
    }
}