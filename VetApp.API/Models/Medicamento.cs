namespace VetApp.API.Models
{
    public class Medicamento
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public double Quantidade { get; set; }
        public string UnidadeDeMedida { get; set; }
        public string ComoSerTomado { get; set; }
        public int ConsultaId { get; set; }
        public Consulta Consulta { get; set; }
    }
}