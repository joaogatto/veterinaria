using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace VetApp.API.Models
{
    public class Cliente
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Endereco { get; set; }
        public string Bairro { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public string OutroNumero { get; set; }
        public string Email { get; set;}
        public byte[] SenhaHash { get; set; }
        public byte[] SenhaSalt { get; set; }
        public ICollection<Animal> Animais { get; set; }
        public ICollection<ClienteClinica> Clinicas { get; set; }
        public ICollection<Agendamento> Agendamentos { get; set; }

        public Cliente()
        {
            this.Clinicas = new Collection<ClienteClinica>();
            this.Animais = new Collection<Animal>();
            this.Agendamentos = new Collection<Agendamento>();
        }
    }
}