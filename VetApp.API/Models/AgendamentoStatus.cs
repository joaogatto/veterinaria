using System;

namespace VetApp.API.Models
{
    public class AgendamentoStatus
    {
        public int Id { get; set; }
        public int AgendamentoId { get; set; }
        public Agendamento Agendamento { get; set; }
        public int StatusId { get; set; }
        public int QuemAlterouId { get; set; }
        public string QuemAlterouRole { get; set; }
        public DateTime DataHoraMudanca { get; set; }
        public string MotivoAlteracao { get; set; }
    }
}