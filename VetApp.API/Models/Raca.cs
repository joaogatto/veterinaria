using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace VetApp.API.Models
{
    public class Raca
    {
        public int Id { get; set; }
        
        [Required]
        public string Nome { get; set; }
        public Especie Especie { get; set; }

        [Required]
        public int EspecieId { get; set; }
        public ICollection<Animal> Animais { get; set; }

        public Raca()
        {
            this.Animais = new Collection<Animal>();
        }
    }
}