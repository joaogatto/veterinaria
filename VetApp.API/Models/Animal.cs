using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace VetApp.API.Models
{
    public class Animal
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public DateTime DataNascimento { get; set; }
        public string Cor { get; set; }
        public Cliente Cliente { get; set; }
        public int ClienteId { get; set; }
        public Raca Raca { get; set; }
        public int RacaId { get; set; }
        public ICollection<Agendamento> Agendamentos { get; set; }
        public ICollection<AnimalPeso> AnimalPesos { get; set; }
        public ICollection<AnimalVacina> AnimalVacinas { get; set; }
        public ICollection<Exame> Exames { get; set; }

        public Animal()
        {
            this.Agendamentos = new Collection<Agendamento>();
            this.AnimalPesos = new Collection<AnimalPeso>();
            this.AnimalVacinas = new Collection<AnimalVacina>();
            this.Exames = new Collection<Exame>();
        }
    }
}