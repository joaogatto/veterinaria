namespace VetApp.API.Models
{
    public class Admin
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public byte[] SenhaHash { get; set; }
        public byte[] SenhaSalt { get; set; }
        public string Role { get; set; }
    }
}