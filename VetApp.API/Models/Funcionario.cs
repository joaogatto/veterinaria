using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace VetApp.API.Models
{
    public class Funcionario
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public byte[] SenhaHash { get; set; }
        public byte[] SenhaSalt { get; set; }
        public string Role { get; set; }
        public int ClinicaId { get; set; }
        public Clinica Clinica { get; set; }

        public ICollection<Exame> Exames { get; set; }

        public Funcionario()
        {
            this.Exames = new Collection<Exame>();
        }
    }
}