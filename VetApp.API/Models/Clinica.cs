using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace VetApp.API.Models
{
    public class Clinica
    {
        public int Id { get; set; }
        public string ClinicaNome { get; set; }
        public string ClinicaLogin { get; set; }
        public string Responsavel { get; set; }
        public string Endereco { get; set; }
        public byte[] SenhaHash { get; set; }
        public byte[] SenhaSalt { get; set; }
        public ICollection<Funcionario> Funcionarios { get; set; }

        public ICollection<ClienteClinica> Clientes { get; set; }
        public ICollection<Agendamento> Agendamentos { get; set; }

        public Clinica()
        {
            this.Funcionarios = new Collection<Funcionario>();
            this.Clientes = new Collection<ClienteClinica>();
            this.Agendamentos = new Collection<Agendamento>();
        }
    }
}