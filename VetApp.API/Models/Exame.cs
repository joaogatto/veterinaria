using System;

namespace VetApp.API.Models
{
    public class Exame
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int AnimalId { get; set; }
        public Animal Animal { get; set; }
        public int FuncionarioId { get; set; }
        public Funcionario Funcionario { get; set; }
        public int? ConsultaId { get; set; }
        public Consulta Consulta { get; set; }
        public DateTime DataHoraExame { get; set; }
        public string Arquivo { get; set; }
        public string Resultado { get; set; }
        public int Status { get; set; }
    }
}