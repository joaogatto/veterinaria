using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace VetApp.API.Models
{
    public class Especie
    {
        public int Id { get; set; }
        
        [Required]
        public string Nome { get; set; }
        public ICollection<Raca> Racas { get; set; }

        public Especie()
        {
            this.Racas = new Collection<Raca>();
        }
    }
}