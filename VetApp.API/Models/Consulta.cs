using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace VetApp.API.Models
{
    public class Consulta
    {
        public int Id { get; set; }
        public int AgendamentoId { get; set; }
        public int AnimalId { get; set; }
        public Animal Animal { get; set; }
        public DateTime DataHoraConsulta { get; set; }
        [MaxLength]
        public string QueixaInicial { get; set; }
        public double? Temperatura { get; set; }
        public double? Peso { get; set; }
        [MaxLength]
        public string ExameClinico { get; set; }
        [MaxLength]
        public string Diagnostico { get; set; }
        public ICollection<ConsultaSintoma> Sintomas { get; set; }
        public ICollection<AnimalVacina> Vacinas { get; set;}
        public ICollection<Exame> Exames { get; set; }
        public ICollection<Medicamento> Medicamentos { get; set; }

        public Consulta()
        {
            this.Sintomas = new Collection<ConsultaSintoma>();
            this.Vacinas = new Collection<AnimalVacina>();
            this.Exames = new Collection<Exame>();
            this.Medicamentos = new Collection<Medicamento>();
        }
    }
}