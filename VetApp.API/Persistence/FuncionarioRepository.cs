using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VetApp.API.Models;

namespace VetApp.API.Persistence
{
    public class FuncionarioRepository : IFuncionarioRepository
    {
        private readonly VetDbContext _context;
        public FuncionarioRepository(VetDbContext context)
        {
            _context = context;
        }
        public async Task<Funcionario> CadastrarFuncionario(Funcionario func, string senha)
        {
            byte[] senhaHash, senhaSalt;
            SenhaCript(senha, out senhaHash, out senhaSalt);

            func.SenhaHash = senhaHash;
            func.SenhaSalt = senhaSalt;
            
            await _context.Funcionarios.AddAsync(func);
            await _context.SaveChangesAsync();

            return func;
        }

        private void SenhaCript(string senha, out byte[] senhaHash, out byte[] senhaSalt)
        {
            using(var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                senhaSalt = hmac.Key;
                senhaHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(senha));
            }
        }

        public async Task<Funcionario> LogarFuncionario(string clinica, string senhaClinica, string email, string senha)
        {
            var clinicaExistente = await _context.Clinicas.FirstOrDefaultAsync(c => c.ClinicaLogin == clinica);
            if(clinicaExistente == null)
                return null;

            if(!VerificarSenha(senhaClinica, clinicaExistente.SenhaHash, clinicaExistente.SenhaSalt))
                return null;               

            var func = await _context.Funcionarios
                .FirstOrDefaultAsync(f => f.Email == email && f.ClinicaId == clinicaExistente.Id );
            if(func == null)
                return null;

            if(!VerificarSenha(senha, func.SenhaHash, func.SenhaSalt))
                return null;
            
            return func;
        }

        private bool VerificarSenha(string senha, byte[] senhaHash, byte[] senhaSalt)
        {
            using(var hmac = new System.Security.Cryptography.HMACSHA512(senhaSalt))
            {
                var senhaComputada = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(senha));
                for(int i = 0; i < senhaComputada.Length; i++)
                {
                    if(senhaComputada[i] != senhaHash[i])
                        return false;
                }
            }
            return true;
        }

        public async Task<bool> FuncionarioExistente(string email)
        {
            if(await _context.Funcionarios.AnyAsync(f => f.Email == email))
                return true;

            return false;
        }

        public async Task<Cliente> CadastrarCliente(Cliente cliente, string senha, int clinicaId)
        {
            byte[] senhaHash, senhaSalt;
            SenhaCript(senha, out senhaHash, out senhaSalt);
            
            cliente.SenhaHash = senhaHash;
            cliente.SenhaSalt = senhaSalt;
            
            var clinica = await _context.Clinicas.FirstOrDefaultAsync(c => c.Id == clinicaId);

            if(clinica == null)
                return null;
            
            await _context.Clientes.AddAsync(cliente);
            await _context.AddRangeAsync(new ClienteClinica { Cliente = cliente, Clinica = clinica });
            await _context.SaveChangesAsync();

            return cliente;
        }

        public async Task<bool> ClienteExistente(string email)
        {
            if(await _context.Clientes.AnyAsync(c => c.Email == email))
                return true;

            return false;
        }

        public async Task<IEnumerable<Cliente>> GetClientes(int clinicaId)
        {
            if(await _context.Clinicas.FirstOrDefaultAsync(c => c.Id == clinicaId) == null)
                return null;

            var clientes = await _context.Clientes.Include(c => c.Animais).Where(c => c.Clinicas.Any(cc => cc.ClinicaId == clinicaId)).ToListAsync();

            return clientes;
        }

        public async Task<ClienteClinica> VincularCliente(int clinicaId, Cliente cliente)
        {

            var vinculoExistente = await _context.ClienteClinica.FirstOrDefaultAsync(clicli =>
                clicli.ClinicaId == clinicaId && clicli.ClienteId == cliente.Id);

            if (vinculoExistente != null) {
                return null;
            }

            var clinica = await _context.Clinicas.FirstOrDefaultAsync(c => c.Id == clinicaId);
            var CliCli = new ClienteClinica { Cliente = cliente , Clinica = clinica};
            await _context.AddRangeAsync(CliCli);
            await _context.SaveChangesAsync();

            return CliCli;
        }

        public async Task<Cliente> ClienteVinculavel(string email, string senha)
        {
            var cliente = await _context.Clientes.FirstOrDefaultAsync(cli => cli.Email == email);

            if(cliente == null)
                return null;
            
            if(!VerificarSenha(senha, cliente.SenhaHash, cliente.SenhaSalt))
                return null;

            return cliente;
        }

        public async Task<Cliente> GetCliente(int clienteId, int clinicaId)
        {
            var cliente = await _context.Clientes
                            .Include(c => c.Animais)
                            .FirstOrDefaultAsync(c => c.Id == clienteId);

            var vinculoExiste = await _context.ClienteClinica
                                    .FirstOrDefaultAsync(cc => cc.ClienteId == clienteId && cc.ClinicaId == clinicaId);

            if(cliente == null || vinculoExiste == null)
                return null;

            return cliente;
        }

        public async Task<bool> SaveAll()
        {
            if (await _context.SaveChangesAsync() > 0)
                return true;

            return false;
        }

        public async Task<bool> ClienteIdExiste(int id)
        {
            if(await _context.Clientes.AnyAsync(c => c.Id == id))
                return true;

            return false;
        }

        public async Task<IEnumerable<Funcionario>> GetFuncionarios(int clinicaId)
        {
            if(await _context.Clinicas.FirstOrDefaultAsync(c => c.Id == clinicaId) == null)
                return null;
            
            var funcionarios = await _context.Funcionarios.Where(f => f.ClinicaId == clinicaId).ToListAsync();
            
            return funcionarios;
        }

        public async Task<Cliente> AtualizaCliente(Cliente cliente)
        {
            var cli = await _context.Clientes.FirstOrDefaultAsync(c => c.Id == cliente.Id);

            if (cli == null)
                return null;

            cli = cliente;

            await _context.SaveChangesAsync();

            return cli;
        }
    }
}