using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using VetApp.API.Models;

namespace VetApp.API.Persistence
{
    public class Seed
    {
        private readonly VetDbContext _context;
        public Seed(VetDbContext context)
        {
            _context = context;
        }

        public void SeedAdmin()
        {
            var adminData = System.IO.File.ReadAllText("C:/projetos/VetApp/VetApp.API/Persistence/AdminSeedData.json");

            var admins = JsonConvert.DeserializeObject<List<Admin>>(adminData);

            foreach (var admin in admins)
            {
                byte[] SenhaHash, SenhaSalt;

                SenhaCript("1234", out SenhaHash, out SenhaSalt);

                admin.SenhaHash = SenhaHash;
                admin.SenhaSalt = SenhaSalt;
                admin.Email = admin.Email.ToLower();
                admin.Role = "Admin";

                _context.Admins.Add(admin);
            }
            _context.SaveChanges();
        }

        private void SenhaCript(string senha, out byte[] senhaHash, out byte[] senhaSalt)
        {
            using(var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                senhaSalt = hmac.Key;
                senhaHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(senha));
            }
        }
    }
}