using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VetApp.API.Dtos;
using VetApp.API.Models;

namespace VetApp.API.Persistence
{
    public interface IAgendamentoRepository
    {
        Task<Agendamento> CadastrarAgendamento(Agendamento agendamento, AlteracaoDto alteracao);
        Task<AgendamentoStatus> MudaStatus(AlteracaoDto alteracao, int agendamentoId, string motivo);

        Task<bool> PodeAlterarAgendamento(int clinicaId, int agendamentoId);
        Task<bool> PodeAgendar(int clinicaId, int clienteId, int animalId);

        Task<IEnumerable<Agendamento>> GetAgendamentosClinica(int clinicaId);
        Task<IEnumerable<Agendamento>> GetAgendamentosFunc(int clinicaId, int funcId);
        Task<IEnumerable<Agendamento>> GetAgendamentosDoDia(int clinicaId);
        Task<IEnumerable<Agendamento>> GetAgendamentosPorData(int clinicaId, DateTime data);
        Task<Agendamento> GetAgendamento(int clinicaId, int agendamentoId);
        Task<IEnumerable<AgendamentoToListDto>> GetStatusAgendamentos(IEnumerable<AgendamentoToListDto> agendamentos);
    }
}