using Microsoft.EntityFrameworkCore;
using VetApp.API.Models;

namespace VetApp.API.Persistence
{
    public class VetDbContext : DbContext
    {
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Funcionario> Funcionarios { get; set; }
        public DbSet<Clinica> Clinicas { get; set; }
        public DbSet<ClienteClinica> ClienteClinica { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Especie> Especies { get; set; }
        public DbSet<Raca> Racas { get; set; }
        public DbSet<Animal> Animais { get; set; }
        public DbSet<AnimalPeso> AnimalPesos { get; set; }
        public DbSet<AnimalVacina> AnimaisVacinas { get; set; }
        public DbSet<Vacina> Vacinas { get; set; }
        public DbSet<Agendamento> Agendamentos { get; set; }
        public DbSet<AgendamentoStatus> AgendamentoStatus { get; set; }
        public DbSet<Consulta> Consultas { get; set; }
        public DbSet<Sintoma> Sintomas { get; set; }
        public DbSet<ConsultaSintoma> ConsultaSintoma { get; set; }
        public DbSet<Exame> Exames { get; set; }
        public DbSet<Medicamento> Medicamentos { get; set; }
        
        public VetDbContext(DbContextOptions<VetDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ClienteClinica>()
                .HasKey(cc => new { cc.ClinicaId, cc.ClienteId });

            modelBuilder.Entity<ConsultaSintoma>()
                .HasKey(consin => new { consin.ConsultaId, consin.SintomaId});
        }

    }
}