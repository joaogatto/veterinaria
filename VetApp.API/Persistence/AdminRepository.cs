using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VetApp.API.Dtos;
using VetApp.API.Models;

namespace VetApp.API.Persistence
{
    public class AdminRepository : IAdminRepository
    {
        private readonly VetDbContext _context;
        public AdminRepository(VetDbContext context)
        {
            _context = context;
        }

        public async Task<Clinica> CadastrarClinica(Clinica clinica, string senha)
        {
            byte[] senhaHash, senhaSalt;
            SenhaCript(senha, out senhaHash, out senhaSalt);

            clinica.SenhaHash = senhaHash;
            clinica.SenhaSalt = senhaSalt;
            
            await _context.Clinicas.AddAsync(clinica);
            await _context.SaveChangesAsync();

            return clinica;
        }

        private void SenhaCript(string senha, out byte[] senhaHash, out byte[] senhaSalt)
        {
            using(var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                senhaSalt = hmac.Key;
                senhaHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(senha));
            }
        }

        public async Task<Admin> LogarAdmin(string email, string senha)
        {
            var admin = await _context.Admins.FirstOrDefaultAsync(adm => adm.Email == email);

            if(admin == null)
                return null;

            if(!VerificarSenha(senha, admin.SenhaHash, admin.SenhaSalt))
                return null;


            return admin;
        }

        private bool VerificarSenha(string senha, byte[] senhaHash, byte[] senhaSalt)
        {
            using(var hmac = new System.Security.Cryptography.HMACSHA512(senhaSalt))
            {
                var senhaComputada = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(senha));
                for(int i = 0; i < senhaComputada.Length; i++)
                {
                    if(senhaComputada[i] != senhaHash[i])
                        return false;
                }
            }
            return true;
        }

        public async Task<bool> ClinicaExiste(string loginClinica)
        {
            if(await _context.Clinicas.AnyAsync(c => c.ClinicaLogin == loginClinica))
                return true;

            return false;
        }

        public async Task<Vacina> CadastrarVacina(string nome)
        {
            var vacinaExiste = await _context.Vacinas.FirstOrDefaultAsync(v => v.Nome == nome);

            if(vacinaExiste != null)
                return null;
            
            var vacina = new Vacina();
            vacina.Nome = nome;

            await _context.Vacinas.AddAsync(vacina);
            await _context.SaveChangesAsync();

            return vacina;
        }
    }
}