using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VetApp.API.Models;

namespace VetApp.API.Persistence
{
    public class VacinaRepository : IVacinasRepository
    {
        private readonly VetDbContext _context;
        public VacinaRepository(VetDbContext context)
        {
            _context = context;

        }

        public async Task<AnimalVacina> aplicarVacina(AnimalVacina animalVacina)
        {
            var aplicacao = await _context.AnimaisVacinas.AddAsync(animalVacina);

            if (aplicacao == null)
                return null;

            await _context.SaveChangesAsync();

            var aplicacaoToReturn = await _context.AnimaisVacinas
                                        .Include(a => a.Vacina)
                                        .FirstOrDefaultAsync(a => a.Id == animalVacina.Id);

            return aplicacaoToReturn;
        }

        public async Task<IEnumerable<Vacina>> getVacinas()
        {
            var vacinas = await _context.Vacinas.ToListAsync();

            if(vacinas == null)
                return null;

            return vacinas;
        }

        public async Task<IEnumerable<AnimalVacina>> getVacinasSemConsulta(int animalId, int funcionarioId)
        {
            var vacinas = await _context.AnimaisVacinas
                            .Include(av => av.Animal)
                            .Include(av => av.Funcionario)
                            .Include(av => av.Vacina)
                            .OrderByDescending(av => av.DataHoraAplicacao)
                            .Where(av => av.AnimalId == animalId && av.ConsultaId == null && av.FuncionarioId == funcionarioId)
                            .ToListAsync();

            if(vacinas == null)
                return null;

            return vacinas;
        }
    }
}