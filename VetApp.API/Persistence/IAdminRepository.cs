using System.Threading.Tasks;
using VetApp.API.Models;

namespace VetApp.API.Persistence
{
    public interface IAdminRepository
    {
         Task<Admin> LogarAdmin(string email, string senha);
         Task<Clinica> CadastrarClinica(Clinica clinica, string senha);
         Task<bool> ClinicaExiste(string loginClinica);
         Task<Vacina> CadastrarVacina(string nome);
    }
}