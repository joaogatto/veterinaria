using System.Collections.Generic;
using System.Threading.Tasks;
using VetApp.API.Models;

namespace VetApp.API.Persistence
{
    public interface IVacinasRepository
    {
         Task<IEnumerable<Vacina>> getVacinas();
         Task<IEnumerable<AnimalVacina>> getVacinasSemConsulta(int animalId, int funcionarioId);

         Task<AnimalVacina> aplicarVacina(AnimalVacina animalVacina);
    }
}