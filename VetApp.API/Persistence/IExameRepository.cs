using System.Threading.Tasks;
using VetApp.API.Models;

namespace VetApp.API.Persistence
{
    public interface IExameRepository
    {
         Task<Exame> CadastrarExame(Exame exame);
         Task<Exame> GetExame(int exameId);
    }
}