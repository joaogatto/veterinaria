using System.Collections.Generic;
using System.Threading.Tasks;
using VetApp.API.Dtos;
using VetApp.API.Models;

namespace VetApp.API.Persistence
{
    public interface IConsultaRepository
    {
         Task<int> CadastrarConsulta(Consulta consulta);
         Task<bool> SintomasCadastrados(ICollection<SintomaDto> sintomas);
         Task<IEnumerable<ConsultaSintoma>> GetConsultaSintomas(ICollection<SintomaDto> sintomas);
         Task<Animal> GetAnimalToConsulta(int animalId);
         Task<bool> CadastrarVacinasDaConsulta(int consultaId, ICollection<VetApp.API.Dtos.KeyValuePair> vacinas);
         Task<bool> CadastrarExamesDaConsulta(int consultaId, ICollection<VetApp.API.Dtos.KeyValuePair> exames);
         Task<bool> CadastrarReceitaDaConsulta(IEnumerable<Medicamento> medicamentos);
    }
}