using System.Collections.Generic;
using System.Threading.Tasks;
using VetApp.API.Models;

namespace VetApp.API.Persistence
{
    public interface IAnimalRepository
    {
        Task<Especie> CadastrarEspecie(Especie especie);         
        Task<IEnumerable<Especie>> GetEspecies();
        Task<bool> EspecieExistente(string nomeEspecie);
        Task<Raca> CadastrarRaca(Raca raca);
        Task<IEnumerable<Raca>> GetRacas();
        Task<bool> TestarEspecieRaca(string nomeRaca, int especieId);
        Task<bool> RacaIdExiste(int id);
        Task<Animal> CadastrarAnimal(Animal animal);
        Task<AnimalPeso> CadastrarPeso(AnimalPeso animalPeso);
    }
}