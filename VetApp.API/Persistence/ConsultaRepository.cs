using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VetApp.API.Dtos;
using VetApp.API.Models;

namespace VetApp.API.Persistence
{
    public class ConsultaRepository : IConsultaRepository
    {
        private readonly VetDbContext _context;
        public ConsultaRepository(VetDbContext context)
        {
            _context = context;

        }
        public async Task<int> CadastrarConsulta(Consulta consulta)
        {
            await _context.AddAsync(consulta);
            await _context.SaveChangesAsync();

            return consulta.Id;
        }

        public async Task<Animal> GetAnimalToConsulta(int animalId)
        {
            var animal = await _context.Animais
                            .Include(a => a.Cliente)
                            .Include(a => a.Raca)
                            .FirstOrDefaultAsync(a => a.Id == animalId);

            if (animal == null)
                return null;
            
            return animal;
        }

        public async Task<IEnumerable<ConsultaSintoma>> GetConsultaSintomas(ICollection<SintomaDto> sintomas)
        {
            List<ConsultaSintoma> sintomasFromRepo = new List<ConsultaSintoma>();

            foreach (var sintoma in sintomas)
            {
                var novoSintoma = await _context.Sintomas.FirstOrDefaultAsync(sin => sin.Descricao == sintoma.Descricao);
                var consultaSintoma = new ConsultaSintoma();
                consultaSintoma.SintomaId = novoSintoma.Id;
                sintomasFromRepo.Add(consultaSintoma);
            }

            return sintomasFromRepo;
        }

        public async Task<bool> SintomasCadastrados(ICollection<SintomaDto> sintomas)
        {
            foreach (var sintoma in sintomas)
            {
                var sintomaExiste = await _context.Sintomas
                    .FirstOrDefaultAsync(s => s.Descricao == sintoma.Descricao);

                if(sintomaExiste == null) {
                    var novoSintoma = new Sintoma();
                    novoSintoma.Descricao = sintoma.Descricao;
                    await _context.Sintomas.AddAsync(novoSintoma);
                    await _context.SaveChangesAsync();
                }
            }

            return true;
        }

        public async Task<bool> CadastrarVacinasDaConsulta(int consultaId, ICollection<VetApp.API.Dtos.KeyValuePair> vacinas)
        {
            var consulta = await _context.Consultas.FirstOrDefaultAsync(c => c.Id == consultaId);

            if (consulta == null) {
                return false;
            }

            foreach (var vacina in vacinas)
            {
                var vacinaFromRepo = await _context.AnimaisVacinas.FirstOrDefaultAsync(av => av.Id == vacina.Id);

                vacinaFromRepo.ConsultaId = consultaId;               
            }

            
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> CadastrarExamesDaConsulta(int consultaId, ICollection<Dtos.KeyValuePair> exames)
        {
            var consulta = await _context.Consultas.FirstOrDefaultAsync(c => c.Id == consultaId);

            if (consulta == null) {
                return false;
            }

            foreach (var exame in exames)
            {
                var exameFromRepo = await _context.Exames.FirstOrDefaultAsync(ex => ex.Id == exame.Id);

                exameFromRepo.ConsultaId = consultaId;               
            }

            
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> CadastrarReceitaDaConsulta(IEnumerable<Medicamento> medicamentos)
        {
            foreach (var medicamento in medicamentos)
            {
                await _context.Medicamentos.AddAsync(medicamento);
            }

            await _context.SaveChangesAsync();

            return true;
        }
    }
}