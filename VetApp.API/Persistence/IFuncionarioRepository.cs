using System.Collections.Generic;
using System.Threading.Tasks;
using VetApp.API.Models;

namespace VetApp.API.Persistence
{
    public interface IFuncionarioRepository
    {
         Task<Funcionario> CadastrarFuncionario(Funcionario func, string senha);
         Task<Funcionario> LogarFuncionario(string clinica, string senhaClinica, string email, string senha);
         Task<IEnumerable<Funcionario>> GetFuncionarios(int clinicaId);
         Task<bool> FuncionarioExistente(string email);
         Task<bool> ClienteExistente(string email);
         Task<bool> ClienteIdExiste(int id);
         Task<Cliente> CadastrarCliente(Cliente cliente, string senha, int clinicaId);
         Task<IEnumerable<Cliente>> GetClientes(int clinicaId);
         Task<Cliente> ClienteVinculavel(string email, string senha);
         Task<ClienteClinica> VincularCliente(int clinicaId, Cliente cliente);
         Task<Cliente> GetCliente(int clienteId, int clinicaId);

         Task<Cliente> AtualizaCliente(Cliente cliente);
         Task<bool> SaveAll();
    }
}