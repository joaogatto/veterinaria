using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VetApp.API.Models;

namespace VetApp.API.Persistence
{
    public class ExameRepository : IExameRepository
    {
        private readonly VetDbContext _context;
        public ExameRepository(VetDbContext context)
        {
            _context = context;

        }

        public async Task<Exame> CadastrarExame(Exame exame)
        {
            await _context.Exames.AddAsync(exame);
            await _context.SaveChangesAsync();

            return exame;
        }

        public async Task<Exame> GetExame(int exameId)
        {
            var exame = await _context.Exames.FirstOrDefaultAsync(e => e.Id == exameId);

            if(exame == null)
                return null;


            return exame;
        }
    }
}