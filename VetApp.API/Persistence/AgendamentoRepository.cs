using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VetApp.API.Dtos;
using VetApp.API.Models;

namespace VetApp.API.Persistence
{
    public class AgendamentoRepository : IAgendamentoRepository
    {
        private readonly VetDbContext _context;
        public AgendamentoRepository(VetDbContext context)
        {
            _context = context;

        }
        public async Task<Agendamento> CadastrarAgendamento(Agendamento agendamento, AlteracaoDto alteracao)
        {
            await _context.Agendamentos.AddAsync(agendamento);
            
            await this.MudaStatus(alteracao, agendamento.Id, "cadastro");
            
            await _context.SaveChangesAsync();

            return agendamento;
        }

        public async Task<Agendamento> GetAgendamento(int clinicaId, int agendamentoId)
        {
            var agendamento = await _context.Agendamentos
                .Include(a => a.Cliente)
                .Include(a => a.Animal)
                    .ThenInclude(animal => animal.Raca)
                .FirstOrDefaultAsync(a => a.ClinicaId == clinicaId && a.Id == agendamentoId);

            if (agendamento == null)
                return null;

            return agendamento;
        }

        public async Task<IEnumerable<Agendamento>> GetAgendamentosClinica(int clinicaId)
        {
            var clinica = await _context.Clinicas.FirstOrDefaultAsync(c => c.Id == clinicaId);
            
            if(clinica == null)
                return null;

            var agendamentos = await _context.Agendamentos
                .Include(a => a.Animal)
                .Include(a => a.Cliente)
                .Include(a => a.Funcionario)
                .Where(a => a.ClinicaId == clinica.Id)
                .OrderByDescending(a => a.DataHora).ToListAsync();

            return agendamentos;
        }

        public async Task<IEnumerable<Agendamento>> GetAgendamentosDoDia(int clinicaId)
        {
            var clinica = await _context.Clinicas.FirstOrDefaultAsync(c => c.Id == clinicaId);
            var hoje = DateTime.Today.Date;
            
            if(clinica == null)
                return null;

            var agendamentos = await _context.Agendamentos
                .Include(a => a.Animal)
                .Include(a => a.Cliente)
                .Include(a => a.Funcionario)
                .Where(a => a.ClinicaId == clinica.Id && a.DataHora.Date == hoje)
                .OrderByDescending(a => a.DataHora).ToListAsync();

            return agendamentos;
        }

        public async Task<IEnumerable<Agendamento>> GetAgendamentosFunc(int clinicaId, int funcId)
        {
            var clinica = await _context.Clinicas.FirstOrDefaultAsync(c => c.Id == clinicaId);
            
            if(clinica == null)
                return null;

            var agendamentos = await _context.Agendamentos
                .Include(a => a.Animal)
                .Include(a => a.Cliente)
                .Include(a => a.Funcionario)
                .Where(a => a.ClinicaId == clinica.Id && a.FuncionarioId == funcId)
                .OrderByDescending(a => a.DataHora).ToListAsync();

            return agendamentos;
        }

        public async Task<IEnumerable<Agendamento>> GetAgendamentosPorData(int clinicaId, DateTime data)
        {
            var clinica = await _context.Clinicas.FirstOrDefaultAsync(c => c.Id == clinicaId);
            var dataSemHora = data.Date;
            
            if(clinica == null)
                return null;

            var agendamentos = await _context.Agendamentos
                .Include(a => a.Animal)
                .Include(a => a.Cliente)
                .Include(a => a.Funcionario)
                .Where(a => a.ClinicaId == clinica.Id && a.DataHora.Date == dataSemHora)
                .OrderByDescending(a => a.DataHora).ToListAsync();

            return agendamentos;
        }

        public async Task<IEnumerable<AgendamentoToListDto>> GetStatusAgendamentos(IEnumerable<AgendamentoToListDto> agendamentos)
        {
            foreach (var agendamento in agendamentos)
            {
                var agendamentoStatus = await _context.AgendamentoStatus
                                        .Where(ags => ags.AgendamentoId == agendamento.Id)
                                        .OrderByDescending(ags => ags.Id)
                                        .FirstOrDefaultAsync();
                if(agendamentoStatus != null) {
                agendamento.StatusId = agendamentoStatus.StatusId;
                }
            }

            return agendamentos;
        }

        public async Task<AgendamentoStatus> MudaStatus(AlteracaoDto alteracao, int agendamentoId, string motivo)
        {
            AgendamentoStatus status = new AgendamentoStatus();
            status.AgendamentoId = agendamentoId;
            status.QuemAlterouId = alteracao.QuemAlterouId;
            status.QuemAlterouRole = alteracao.QuemAlterouRole;
            if(motivo == "cadastro" || motivo == "alteracao") {
                status.StatusId = 1;
            } else if (motivo == "consulta realizada") {
                status.StatusId = 3;
                var consulta = await _context.Consultas
                                .FirstOrDefaultAsync(c => c.AgendamentoId == agendamentoId);
                motivo = consulta.Id.ToString();
            } else if (motivo == "finalizado") {
                status.StatusId = 3;

            } else if (motivo == "nao compareceu" || motivo == "outro") {
                status.StatusId = 4;
            }
            status.DataHoraMudanca = DateTime.Now;
            status.MotivoAlteracao = motivo;

            await _context.AgendamentoStatus.AddAsync(status);
            
            await _context.SaveChangesAsync();

            return status;
        }

        public async Task<bool> PodeAgendar(int clinicaId, int clienteId, int animalId)
        {
            var cliente = await _context.Clientes
                .Include(c => c.Clinicas)
                .Include(c => c.Animais)
                .FirstOrDefaultAsync(cli => cli.Id == clienteId);

            if(cliente != null) {
                foreach (var clinica in cliente.Clinicas)
                {
                    if(clinica.ClinicaId == clinicaId) {
                        foreach (var animal in cliente.Animais)
                        {
                            if (animalId == animal.Id)
                                return true;
                        }
                    }
                }
            }

            return false;
        }

        public async Task<bool> PodeAlterarAgendamento(int clinicaId, int agendamentoId)
        {
            var agendamento = await _context.Agendamentos
                                .Include(a => a.AgendamentoStatus)
                                .Where(a => a.ClinicaId == clinicaId)
                                .FirstOrDefaultAsync(a => a.Id == agendamentoId);

            if(agendamento == null)
                return false;

            var status = agendamento.AgendamentoStatus.Last().StatusId;

            if((status == 3 || status == 4)) {
                return false;
            }

            return true;
        }
    }
}