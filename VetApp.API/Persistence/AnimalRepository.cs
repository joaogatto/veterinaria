using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using VetApp.API.Models;

namespace VetApp.API.Persistence
{
    public class AnimalRepository : IAnimalRepository
    {
        private readonly VetDbContext _context;
        public AnimalRepository(VetDbContext context)
        {
            _context = context;

        }

        public async Task<Animal> CadastrarAnimal(Animal animal)
        {
            await _context.Animais.AddAsync(animal);

            await _context.SaveChangesAsync();

            return animal;
        }

        public async Task<Especie> CadastrarEspecie(Especie especie)
        {         
            await _context.Especies.AddAsync(especie);
            await _context.SaveChangesAsync();

            return especie;
        }

        public async Task<Raca> CadastrarRaca(Raca raca)
        {
            await _context.Racas.AddAsync(raca);
            await _context.SaveChangesAsync();

            return raca;
        }

        public async Task<bool> EspecieExistente(string nomeEspecie)
        {
            if(await _context.Especies.AnyAsync(e => e.Nome == nomeEspecie))
                return true;
            
            return false;
        }

        public async Task<IEnumerable<Especie>> GetEspecies()
        {
            var especies = await _context.Especies
                .Include(e => e.Racas)
                .ToListAsync();

            especies = await _context.Especies.ToListAsync();

            return especies;
        }

        public async Task<IEnumerable<Raca>> GetRacas()
        {
            var racas = await _context.Racas.Include(r => r.Especie).ToListAsync();

            return racas;
        }

        public async Task<bool> TestarEspecieRaca(string nomeRaca, int especieId)
        {
            var especie = await _context.Especies.FirstOrDefaultAsync(e => e.Id == especieId);

            if(especie == null)
                return true;

            var racaExistente = await _context.Racas
                .FirstOrDefaultAsync(r => r.EspecieId == especie.Id && r.Nome == nomeRaca);

            if(racaExistente != null)
                return true;
            
            return false;
        }

        public async Task<bool> RacaIdExiste(int id)
        {
            if(await _context.Racas.AnyAsync(r => r.Id == id))
                return true;
                
            return false;
        }

        public async Task<AnimalPeso> CadastrarPeso(AnimalPeso animalPeso)
        {
            await _context.AnimalPesos.AddAsync(animalPeso);
            await _context.SaveChangesAsync();

            return animalPeso;
        }
    }
}