using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VetApp.API.Dtos;
using VetApp.API.Models;
using VetApp.API.Persistence;

namespace VetApp.API.Controllers
{
    [Authorize(Roles = "Admin,Gerente,Veterinario")]
    [Route("api/[controller]")]
    [ApiController]
    public class AgendamentosController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IAgendamentoRepository _agendaRepo;
        private readonly IFuncionarioRepository _funcRepo;
        private readonly IHostingEnvironment _host;

        public AgendamentosController(IAgendamentoRepository agendaRepo,
            IMapper mapper,
            IFuncionarioRepository funcRepo,
            IHostingEnvironment host)
        {
            _funcRepo = funcRepo;
            _host = host;
            _agendaRepo = agendaRepo;
            _mapper = mapper;

        }

        [HttpGet("getAgendamentos")]
        public async Task<IActionResult> getAgendamentos()
        {
            var clinicaId = Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == "clinicaId").Value);
            var agendamentosRepo = await _agendaRepo.GetAgendamentosClinica(clinicaId);

            if (agendamentosRepo == null)
                return BadRequest("Clinica não encontrada");

            var agendamentosMap = _mapper.Map<IEnumerable<Agendamento>, IEnumerable<AgendamentoToListDto>>(agendamentosRepo);

            var agendamentosRetorno = await _agendaRepo.GetStatusAgendamentos(agendamentosMap);
            
            return Ok(agendamentosRetorno);
        }

        [HttpGet("getAgendamentosDoDia")]
        public async Task<IActionResult> getAgendamentosDoDia()
        {
            var clinicaId = Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == "clinicaId").Value);
            var agendamentosRepo = await _agendaRepo.GetAgendamentosDoDia(clinicaId);

            if (agendamentosRepo == null)
                return BadRequest("Clinica não encontrada");

            var agendamentosRetorno = _mapper.Map<IEnumerable<Agendamento>, IEnumerable<AgendamentoToListDto>>(agendamentosRepo);

            return Ok(agendamentosRetorno);
        }

        [HttpGet("getAgendamentoConsulta/{agendamentoId}")]
        public async Task<IActionResult> getAgendamentoConsulta(int agendamentoId)
        {
            var clinicaId = Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == "clinicaId").Value);

            var agendamentoFromRepo = await _agendaRepo.GetAgendamento(clinicaId, agendamentoId);

            if (agendamentoFromRepo == null)
                return BadRequest("Agendamento não encontrado");

            var agendamentoRetorno = _mapper.Map<Agendamento, AgendamentoToConsultaDto>(agendamentoFromRepo);

            return Ok(agendamentoRetorno);
        }

        [HttpPost("cadastrarAgendamento")]
        public async Task<IActionResult> CadastrarAgendamento(AgendamentoFromClinicaDto agendamentoFromClinicaDto)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            agendamentoFromClinicaDto.ClinicaId = Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == "clinicaId").Value);
            var agendamento = _mapper.Map<AgendamentoFromClinicaDto, Agendamento>(agendamentoFromClinicaDto);
            var clinicaId = agendamentoFromClinicaDto.ClinicaId;

            var status = new AlteracaoDto();
            status.QuemAlterouId = Int32.Parse(@User.Claims.FirstOrDefault(
                c => c.Type == ClaimTypes.NameIdentifier).Value);
            status.QuemAlterouRole = @User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;

            if (!await _agendaRepo.PodeAgendar(clinicaId, agendamento.ClienteId, agendamento.AnimalId))
            {
                return BadRequest("Não foi possivel realizar o agendamento.");
            }

            var agendamentoFromRepo = await _agendaRepo.CadastrarAgendamento(agendamento, status);

            if (agendamentoFromRepo == null)
                return BadRequest("Falha ao cadastrar agendamento");

            var agendamentoRetorno = _mapper.Map<Agendamento, AgendamentoToListDto>(agendamentoFromRepo);

            return Ok(agendamentoRetorno);
        }

        [HttpPut("atualizarAgendamento/{agendamentoId}")]
        public async Task<IActionResult> AtualizarAgendamento(int agendamentoId, AgendamentoFromClinicaDto agendamento)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var clinicaId = Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == "clinicaId").Value); 

            agendamento.ClinicaId = clinicaId;

            if(! await _agendaRepo.PodeAlterarAgendamento(clinicaId, agendamentoId))
            {
                return BadRequest("Não é possível alterar agendamento já finalizado");
            }
            

            var status = new AlteracaoDto();
            status.QuemAlterouId = Int32.Parse(@User.Claims.FirstOrDefault(
                c => c.Type == ClaimTypes.NameIdentifier).Value);
            status.QuemAlterouRole = @User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;

            if (!await _agendaRepo.PodeAgendar(clinicaId, agendamento.ClienteId, agendamento.AnimalId))
            {
                return BadRequest("Não foi possivel realizar o agendamento.");
            }

            var agendamentoFromRepo = await _agendaRepo.GetAgendamento(clinicaId, agendamentoId);

            _mapper.Map(agendamento, agendamentoFromRepo);

            if (await _funcRepo.SaveAll()) {
                await _agendaRepo.MudaStatus(status, agendamentoFromRepo.Id, "alteracao");
                return NoContent();
            }

            throw new Exception("Falha ao atualizar");
        }

        [HttpGet("getAgendamentosPorData/{data}")]
        public async Task<IActionResult> getAgendamentosPorData(double data)
        {
            var clinicaId = Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == "clinicaId").Value);
            DateTime dtFromTimeSpan = new DateTime(1970,1,1).Add(TimeSpan.FromMilliseconds(data));
            var agendamentosRepo = await _agendaRepo.GetAgendamentosPorData(clinicaId, dtFromTimeSpan);

            if (agendamentosRepo == null)
                return BadRequest("Clinica não encontrada");

            var agendamentosRetorno = _mapper.Map<IEnumerable<Agendamento>, IEnumerable<AgendamentoToListDto>>(agendamentosRepo);

            return Ok(agendamentosRetorno);
        }

        [HttpGet("getAgendamentosFunc")]
        public async Task<IActionResult> getAgendamentosFunc()
        {
            var clinicaId = Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == "clinicaId").Value);

            var funcId = Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);

            var agendamentoFromRepo = await _agendaRepo.GetAgendamentosFunc(clinicaId, funcId);

            var agendamentosMap = _mapper.Map<IEnumerable<Agendamento>, IEnumerable<AgendamentoToListDto>>(agendamentoFromRepo);

            var agendamentosRetorno = await _agendaRepo.GetStatusAgendamentos(agendamentosMap);

            return Ok(agendamentosRetorno);
        }

        [HttpPost("cancelarAgendamento")]
        public async Task<IActionResult> cancelarAgendamento(CancelarAgendamentoDto cancelarAgendamentoDto)
        {
            var clinicaId = Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == "clinicaId").Value);

            if(! await _agendaRepo.PodeAlterarAgendamento(clinicaId, cancelarAgendamentoDto.AgendamentoId))
            {
                return BadRequest("Não é possível alterar agendamento já finalizado");
            }

            var status = new AlteracaoDto();
            status.QuemAlterouId = Int32.Parse(@User.Claims.FirstOrDefault(
                c => c.Type == ClaimTypes.NameIdentifier).Value);
            status.QuemAlterouRole = @User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;

            await _agendaRepo.MudaStatus(status, cancelarAgendamentoDto.AgendamentoId, cancelarAgendamentoDto.Motivo);

            return Ok();
        }

        [HttpPut("finalizarAgendamento/{agendamentoId}")]
        public async Task<IActionResult> FinalizarAgendamento(int agendamentoId, IFormFile fotoArquivo)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var clinicaId = Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == "clinicaId").Value);

            if(! await _agendaRepo.PodeAlterarAgendamento(clinicaId, agendamentoId))
            {
                return BadRequest("Não é possível alterar agendamento já finalizado");
            }

            var status = new AlteracaoDto();
            status.QuemAlterouId = Int32.Parse(@User.Claims.FirstOrDefault(
                c => c.Type == ClaimTypes.NameIdentifier).Value);
            status.QuemAlterouRole = @User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;

            var agendamentoFromRepo = await _agendaRepo.GetAgendamento(clinicaId, agendamentoId);
            var agendamentoFinalizado = new AgendamentoFinalizadoDto();
            agendamentoFinalizado.Id = agendamentoId;
            
            if(fotoArquivo != null) {
                if (fotoArquivo.ContentType != "image/jpeg")
                    return BadRequest("Formato do arquivo inválido");

                string FileName = agendamentoId + fotoArquivo.FileName;
                agendamentoFinalizado.Foto = FileName;
                
                var caminhoPastaUpload = Path.Combine(_host.WebRootPath, "agendamentos/" + agendamentoFromRepo.AnimalId);
                if (!Directory.Exists(caminhoPastaUpload))
                    Directory.CreateDirectory(caminhoPastaUpload);
                
                await fotoArquivo.CopyToAsync(new FileStream(caminhoPastaUpload + "/" + FileName, FileMode.Create));
            }


            _mapper.Map(agendamentoFinalizado, agendamentoFromRepo);


            if (await _funcRepo.SaveAll()) {
                await _agendaRepo.MudaStatus(status, agendamentoFromRepo.Id, "finalizado");
                return NoContent();
            }

            throw new Exception("Falha ao finalizar agendamento");
        }

        private byte[] ConvertIFormFileToBase64(IFormFile file)
        {
            using (BinaryReader reader = new BinaryReader(file.OpenReadStream()))
            {
                byte[] imgByte = reader.ReadBytes((int)file.Length);
                
                return imgByte;
            }
        }
    }
}