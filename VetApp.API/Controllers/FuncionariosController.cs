using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using VetApp.API.Dtos;
using VetApp.API.Models;
using VetApp.API.Persistence;

namespace VetApp.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FuncionariosController : ControllerBase
    {
        private readonly IFuncionarioRepository _funcRepo;
        private readonly IConfiguration _config;
        private readonly IMapper _mapper;

        public FuncionariosController(IFuncionarioRepository repo,
            IConfiguration config,
            IMapper mapper)
        {
            _config = config;
            _mapper = mapper;
            _funcRepo = repo;
        }

        [Authorize(Roles="Gerente")]
        [HttpPost("cadastrarFuncionario")]
        public async Task<IActionResult> Cadastrar(FuncionarioCadastroDto funcionarioCadastroDto)
        {
            funcionarioCadastroDto.ClinicaId = Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == "clinicaId").Value);

            funcionarioCadastroDto.Email = funcionarioCadastroDto.Email.ToLower();

            if (await _funcRepo.FuncionarioExistente(funcionarioCadastroDto.Email))
                return BadRequest("Email já cadastrado!");

            var funcionarioModel = _mapper.Map<Funcionario>(funcionarioCadastroDto);

            var funcionarioCadastrado = await _funcRepo.CadastrarFuncionario(funcionarioModel, funcionarioCadastroDto.Senha);

            return StatusCode(201);
        }

        [Authorize(Roles="Gerente,Funcionario,Veterinario")]
        [HttpPost("cadastrarcliente")]
        public async Task<IActionResult> CadastrarCliente(ClienteCadastroDto clienteCadastroDto)
        {
            clienteCadastroDto.Email = clienteCadastroDto.Email.ToLower();

            if(await _funcRepo.ClienteExistente(clienteCadastroDto.Email))
                return BadRequest("Email já cadastrado");
            
            clienteCadastroDto.ClinicaId = Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == "clinicaId").Value);

            var cliente = _mapper.Map<Cliente>(clienteCadastroDto);

            var cliCadastrado = await _funcRepo.CadastrarCliente(cliente, clienteCadastroDto.Senha, clienteCadastroDto.ClinicaId);

            if(cliCadastrado == null) {
                return BadRequest("Não foi possível realizar o cadastro");
            }

            return Ok();
        }
        
        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Login(FuncionarioLoginDto funcionarioLoginDto)
        {
            var funcRepo = await _funcRepo.LogarFuncionario(
                funcionarioLoginDto.Clinica,
                funcionarioLoginDto.SenhaClinica,
                funcionarioLoginDto.Email,
                funcionarioLoginDto.Senha);

            if (funcRepo == null)
                return Unauthorized();

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, funcRepo.Id.ToString()),
                new Claim("clinicaId", funcRepo.ClinicaId.ToString()),
                new Claim(ClaimTypes.Name, funcRepo.Nome),
                new Claim(ClaimTypes.Role, funcRepo.Role)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8
                .GetBytes(_config.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddHours(12),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return Ok(new {
                token = tokenHandler.WriteToken(token)
            });
        }

        [Authorize(Roles="Gerente,Funcionario,Veterinario")]
        [HttpGet("getClientes")]
        public async Task<IActionResult> getClientes()
        {
            var clinicaId = Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == "clinicaId").Value);
            var clientes = await _funcRepo.GetClientes(clinicaId);

            if(clientes == null)
                return BadRequest("Clinica não encontrada");

            var clientesToReturn = _mapper.Map<IEnumerable<ClienteListaDto>>(clientes); 

            return Ok(clientesToReturn);
        }

        [Authorize(Roles="Gerente,Funcionario,Veterinario")]
        [HttpPost("vincularCliente")]
        public async Task<IActionResult> vincularCliente(VincularClienteDto vincularClienteDto) {

            var cliente = await _funcRepo.ClienteVinculavel(vincularClienteDto.Email, vincularClienteDto.Senha);

            vincularClienteDto.ClinicaId = Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == "clinicaId").Value);
            if(cliente == null)
                return NotFound("Cliente não encontrado ou senha incorreta");

            var clicli = await _funcRepo.VincularCliente(vincularClienteDto.ClinicaId, cliente);

            if(clicli == null)
                return BadRequest("Não foi possível realizar o vinculo, possívelmente o cliente já está vinculado.");

            return Ok();
        }

        [Authorize(Roles="Gerente,Funcionario,Veterinario")]
        [HttpPut("atualizarCliente")]
        public async Task<IActionResult> atualizarCliente(ClienteListaDto cliente) {

            if(!ModelState.IsValid)
                return BadRequest(ModelState);
            var clinicaId = Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == "clinicaId").Value);

            
            var cliFromRepo = await _funcRepo.GetCliente(cliente.Id, clinicaId);

            if(cliFromRepo == null)
                return BadRequest("Cliente não encontrado ou vinculo não existe");


            _mapper.Map(cliente, cliFromRepo);

            if (await _funcRepo.SaveAll())
                return NoContent();

            throw new Exception("Falha ao atualizar");
        }

        [Authorize(Roles="Gerente,Funcionario,Veterinario")]
        [HttpGet("getFuncionariosToAgendamento")]
        public async Task<IActionResult> getFuncionarios() {
            var clinicaId = Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == "clinicaId").Value);

            var funcionarios = await _funcRepo.GetFuncionarios(clinicaId);

            if(funcionarios == null)
                return BadRequest("Erro ao obter funcionários");

            var funcionariosToReturn = _mapper.Map<IEnumerable<Funcionario>, IEnumerable<FuncionarioAgendamentoDto>>(funcionarios);

            return Ok(funcionariosToReturn);
        }

        [Authorize(Roles="Gerente,Funcionario,Veterinario")]
        [HttpGet("getCliente/{clienteId}")]
        public async Task<IActionResult> getCliente(int clienteId) {
            var clinicaId = Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == "clinicaId").Value);

            var cliente = await _funcRepo.GetCliente(clienteId, clinicaId);

            if(cliente == null)
                return BadRequest("Cliente não encontrado ou vinculo não existe");

            var clienteToReturn = _mapper.Map<Cliente, ClienteListaDto>(cliente);

            return Ok(clienteToReturn);
        }
    }
}