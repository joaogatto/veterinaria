using System;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VetApp.API.Dtos;
using VetApp.API.Models;
using VetApp.API.Persistence;

namespace VetApp.API.Controllers
{
    [Authorize(Roles = "Veterinario")]
    [Route("api/[controller]")]
    [ApiController]
    public class ExamesController : ControllerBase
    {
        private readonly IExameRepository _exameRepo;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _host;
        private readonly IFuncionarioRepository _funcRepo;

        public ExamesController(IExameRepository exameRepo,
            IMapper mapper,
            IHostingEnvironment host,
            IFuncionarioRepository funcRepo)
        {
            _host = host;
            _funcRepo = funcRepo;
            _mapper = mapper;
            _exameRepo = exameRepo;
        }

        [HttpPost("cadastrarExame")]
        public async Task<IActionResult> cadastrarExame(ExameFromClinicaDto exameFCDto)
        {

            exameFCDto.FuncionarioId = Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);

            var exame = _mapper.Map<ExameFromClinicaDto, Exame>(exameFCDto);

            var exameFromRepo = await _exameRepo.CadastrarExame(exame);

            if (exameFromRepo == null)
                return BadRequest("Falha ao cadastrar exame");

            var exameToReturn = _mapper.Map<Exame, KeyValuePair>(exameFromRepo);

            return Ok(exameToReturn);
        }

        [HttpPut("vincularArquivoExame/{exameId}")]
        public async Task<IActionResult> vincularArquivoExame(int exameId, IFormFile arquivo)
        {
            var exame = await _exameRepo.GetExame(exameId);

            if (exame == null)
                return NotFound();

            var fileName = exameId.ToString() + arquivo.FileName;

            var exameAtualizado = new ExameVincularArquivoDto();
            exameAtualizado.Arquivo = fileName;

            if (arquivo != null)
            {
                if (arquivo.ContentType != "image/jpeg" && arquivo.ContentType != "application/pdf")
                    return BadRequest("Formato do arquivo inválido");

                var caminhoPastaUpload = Path.Combine(_host.WebRootPath, "exames/" + exame.AnimalId);
                if (!Directory.Exists(caminhoPastaUpload))
                    Directory.CreateDirectory(caminhoPastaUpload);

                await arquivo.CopyToAsync(new FileStream(caminhoPastaUpload + "/" + fileName, FileMode.Create));

                exameAtualizado.Arquivo = fileName;
            }

            _mapper.Map(exameAtualizado, exame);

            if (await _funcRepo.SaveAll()) {
                return NoContent();
            }

            throw new Exception("Falha ao Vincular Arquivo/Exame");    
        }
    }
}