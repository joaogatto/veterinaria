using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VetApp.API.Dtos;
using VetApp.API.Models;
using VetApp.API.Persistence;

namespace VetApp.API.Controllers
{
    [Authorize(Roles = "Veterinario")]
    [Route("api/[controller]")]
    [ApiController]
    public class ConsultasController : ControllerBase
    {
        private readonly IConsultaRepository _consultaRepo;
        private readonly IMapper _mapper;
        private readonly IAgendamentoRepository _agendaRepo;
        private readonly IAnimalRepository _animalRepo;

        public ConsultasController(IConsultaRepository consultaRepo,
        IMapper mapper,
        IAgendamentoRepository agendaRepo,
        IAnimalRepository animalRepo)
        {
            _agendaRepo = agendaRepo;
            _animalRepo = animalRepo;
            _mapper = mapper;
            _consultaRepo = consultaRepo;

        }

        [HttpPost("cadastrarConsulta")]
        public async Task<IActionResult> cadastrarConsulta(ConsultaFromCadastroDto consultaFromCadastro)
        {
            if(!ModelState.IsValid) {
                return BadRequest("Bad Model State");
            }
            await _consultaRepo.SintomasCadastrados(consultaFromCadastro.Sintomas);

            var consulta = _mapper.Map<ConsultaFromCadastroDto, Consulta>(consultaFromCadastro);

            var sintomasFromRepo = await _consultaRepo.GetConsultaSintomas(consultaFromCadastro.Sintomas);

            foreach (var sintoma in sintomasFromRepo)
            {
                consulta.Sintomas.Add(sintoma);
            }

            var consultaIdFromRepo = await _consultaRepo.CadastrarConsulta(consulta);

            if (consultaFromCadastro.AgendamentoId > 0)
            {
                var status = new AlteracaoDto();
                status.QuemAlterouId = Int32.Parse(@User.Claims.FirstOrDefault(
                    c => c.Type == ClaimTypes.NameIdentifier).Value);
                status.QuemAlterouRole = @User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;

                await _agendaRepo.MudaStatus(status, consultaFromCadastro.AgendamentoId, "consulta realizada");
            }

            if(consultaFromCadastro.Peso != null) {
                var animalPeso = _mapper.Map<ConsultaFromCadastroDto, AnimalPeso>(consultaFromCadastro);
                await _animalRepo.CadastrarPeso(animalPeso);
            }

            if(consultaFromCadastro.Vacinas.Any()) {
                var vacinas = consultaFromCadastro.Vacinas;
                await _consultaRepo.CadastrarVacinasDaConsulta(consultaIdFromRepo, consultaFromCadastro.Vacinas);
            }

            if(consultaFromCadastro.Exames.Any()) {
                var exames = consultaFromCadastro.Exames;
                await _consultaRepo.CadastrarExamesDaConsulta(consultaIdFromRepo, consultaFromCadastro.Exames);
            }

            if(consultaFromCadastro.Medicamentos.Any()) {
                var medicamentos = _mapper.Map<IEnumerable<MedicamentosFromConsultaDto>, IEnumerable<Medicamento>>(consultaFromCadastro.Medicamentos);

                foreach (var medicamento in medicamentos)
                {
                    medicamento.ConsultaId = consultaIdFromRepo;
                }

                await _consultaRepo.CadastrarReceitaDaConsulta(medicamentos);
            }

            return Ok(consultaIdFromRepo);
        }

        [HttpGet("getAnimalToConsulta/{animalId}")]
        public async Task<IActionResult> getAnimalToConsulta(int animalId)
        {
            var animal = await _consultaRepo.GetAnimalToConsulta(animalId);

            if(animal == null)
                return NotFound("Não foi possível encontrar o Animal");

            var animalToReturn = _mapper.Map<Animal, AgendamentoToConsultaDto>(animal);

            return Ok(animalToReturn);
        }

    }
}