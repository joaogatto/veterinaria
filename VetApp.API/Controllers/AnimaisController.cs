using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VetApp.API.Dtos;
using VetApp.API.Models;
using VetApp.API.Persistence;

namespace VetApp.API.Controllers
{
    [Authorize(Roles = "Admin,Gerente,Veterinario")]
    [Route("api/[controller]")]
    [ApiController]
    public class AnimaisController : ControllerBase
    {
        private readonly IAnimalRepository _animalRepo;
        private readonly IMapper _mapper;
        private readonly IFuncionarioRepository _funcRepo;
        public AnimaisController(IAnimalRepository animalRepo, IMapper mapper, IFuncionarioRepository funcRepo)
        {
            _funcRepo = funcRepo;
            _mapper = mapper;
            _animalRepo = animalRepo;
        }

        [HttpPost("cadastrarEspecie")]
        public async Task<IActionResult> cadastrarEspecie(EspecieCadastroDto especieCadastroDto)
        {
            especieCadastroDto.Nome = especieCadastroDto.Nome.ToLower();

            if (await _animalRepo.EspecieExistente(especieCadastroDto.Nome))
                return BadRequest("Especie já existe");

            var especie = _mapper.Map<EspecieCadastroDto, Especie>(especieCadastroDto);

            var especieRepo = await _animalRepo.CadastrarEspecie(especie);

            if (especieRepo == null)
                return BadRequest("No return");

            var especieRetorno = _mapper.Map<Especie, Dtos.KeyValuePair>(especieRepo);

            return Ok(especieRetorno);
        }

        [HttpGet("getEspecies")]
        public async Task<IActionResult> getEspecies()
        {
            var especies = await _animalRepo.GetEspecies();

            var especiesReturn = _mapper.Map<IEnumerable<Especie>, IEnumerable<EspecieResource>>(especies);

            return Ok(especiesReturn);
        }

        [HttpPost("cadastrarRaca")]
        public async Task<IActionResult> cadastrarRaca(RacaCadastroDto racaCadastroDto)
        {
            racaCadastroDto.Nome = racaCadastroDto.Nome.ToLower();
            if (await _animalRepo.TestarEspecieRaca(racaCadastroDto.Nome, racaCadastroDto.EspecieId))
                return BadRequest("Raça já existe ou Espécie não encontrada");

            var raca = _mapper.Map<RacaCadastroDto, Raca>(racaCadastroDto);

            var racaRepo = await _animalRepo.CadastrarRaca(raca);

            if(racaRepo == null)
                return BadRequest("No return");

            var racaRetorno = _mapper.Map<Raca, RacaCadastroRetornoDto>(racaRepo);

            return Ok(racaRetorno);
        }

        [HttpGet("getRacas")]
        public async Task<IActionResult> getRacas()
        {
            var racas = await _animalRepo.GetRacas();

            var racasRetorno = _mapper.Map<IEnumerable<Raca>, IEnumerable<RacaResource>>(racas);

            return Ok(racasRetorno);
        }

        [HttpPost("cadastrarAnimal")]
        public async Task<IActionResult> cadastrarAnimal(AnimalCadastroDto animalCadastroDto)
        {
            if(!await _funcRepo.ClienteIdExiste(animalCadastroDto.ClienteId))
                return NotFound("Cliente não encontrado");
            
            if(!await _animalRepo.RacaIdExiste(animalCadastroDto.RacaId))
                return NotFound("Raça não encontrada");

            var animal = _mapper.Map<AnimalCadastroDto, Animal>(animalCadastroDto);

            var animalFromRepo = await _animalRepo.CadastrarAnimal(animal);

            var animalRetorno = _mapper.Map<Animal, Dtos.KeyValuePair>(animalFromRepo);

            return Ok(animalRetorno);        
        }
    }
}