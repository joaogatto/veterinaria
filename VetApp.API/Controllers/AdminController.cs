using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using VetApp.API.Dtos;
using VetApp.API.Models;
using VetApp.API.Persistence;

namespace VetApp.API.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IAdminRepository _repo;
        private readonly IConfiguration _config;
        private readonly IFuncionarioRepository _funcRepo;
        private readonly IMapper _mapper;

        public AdminController(IAdminRepository repo,
            IConfiguration config,
            IFuncionarioRepository funcRepo,
            IMapper mapper)
        {
            _funcRepo = funcRepo;
            _mapper = mapper;
            _config = config;
            _repo = repo;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Login(AdminLoginDto adminLoginDto)
        {
            var adminRepo = await _repo.LogarAdmin(adminLoginDto.Email, adminLoginDto.Senha);

            if (adminRepo == null)
                return Unauthorized();

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, adminRepo.Id.ToString()),
                new Claim(ClaimTypes.Email, adminRepo.Email),
                new Claim(ClaimTypes.Role, adminRepo.Role)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8
                .GetBytes(_config.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddHours(6),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return Ok(new
            {
                token = tokenHandler.WriteToken(token)
            });
        }

        [HttpPost("cadastrarclinica")]
        public async Task<IActionResult> CadastrarClinica(ClinicaCadastroDto clinicaCadastroDto)
        {
            if(await _repo.ClinicaExiste(clinicaCadastroDto.ClinicaLogin))
                return BadRequest("Já existe uma clinica com esse Login");

            var clinicaModel = _mapper.Map<Clinica>(clinicaCadastroDto);

            var clinicaCadastro = await _repo.CadastrarClinica(clinicaModel, clinicaCadastroDto.Senha);
            
            var funcionario = new Funcionario
            {
                ClinicaId = clinicaCadastro.Id,
                Nome = clinicaCadastroDto.Funcionario.Nome,
                Email = clinicaCadastroDto.Funcionario.Email,
                Role = clinicaCadastroDto.Funcionario.Role,
            };

            var funcionarioCadastro = await _funcRepo.CadastrarFuncionario(funcionario, clinicaCadastroDto.Funcionario.Senha);

            return Ok("Clínica cadastrada");
        }

        [HttpPost("cadastrarVacina")]
        public async Task<IActionResult> cadastrarVacina(KeyValuePair vacina)
        {
            var vacinaFromRepo = await _repo.CadastrarVacina(vacina.Nome);

            if(vacina == null)
                return BadRequest("Falha ao cadastrar vacina");

            return Ok(vacinaFromRepo);
        }
    }
}