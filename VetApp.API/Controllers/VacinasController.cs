using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VetApp.API.Dtos;
using VetApp.API.Models;
using VetApp.API.Persistence;

namespace VetApp.API.Controllers
{
    [Authorize(Roles = "Veterinario")]
    [Route("api/[controller]")]
    [ApiController]
    public class VacinasController : Controller
    {
        private readonly IVacinasRepository _vacinaRepo;
        private readonly IMapper _mapper;
        public VacinasController(IVacinasRepository vacinaRepo, IMapper mapper)
        {
            _mapper = mapper;
            _vacinaRepo = vacinaRepo;
        }

        [HttpGet("getVacinas")]
        public async Task<IActionResult> getVacinas()
        {
            var vacinasFromRepo = await _vacinaRepo.getVacinas();

            if (vacinasFromRepo == null)
                return NotFound();

            return Ok(vacinasFromRepo);
        }

        [HttpPost("aplicarVacina")]
        public async Task<IActionResult> aplicarVacina(VacinaAplicacaoDto aplicacao)
        {
            if (!ModelState.IsValid)
                return BadRequest("Bad Model State");

            aplicacao.DataHoraAplicacao = aplicacao.DataHoraAplicacao.ToLocalTime();
            
            aplicacao.FuncionarioId = Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
        
            var aplicacaoMapped = _mapper.Map<VacinaAplicacaoDto, AnimalVacina>(aplicacao);

            var aplicacaoFromRepo = await _vacinaRepo.aplicarVacina(aplicacaoMapped);

            if(aplicacaoFromRepo == null)
                return BadRequest("Falha ao cadastrar");

            var aplicacaoToReturn = _mapper.Map<AnimalVacina, VacinaToConsultaDto>(aplicacaoFromRepo);

            return Ok(aplicacaoToReturn);
        }

        [HttpGet("getVacinasSemConsulta/{animalId}")]
        public async Task<IActionResult> getVacinasSemConsulta(int animalId)
        {
            var funcionarioId = 
                Int32.Parse(@User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);

            var vacinasFromRepo = await _vacinaRepo.getVacinasSemConsulta(animalId, funcionarioId);

            var vacinasToReturn = 
                _mapper.Map<IEnumerable<AnimalVacina>, IEnumerable<AniVacToVinculoDto>>(vacinasFromRepo);

            return Ok(vacinasToReturn);
        }
    }
}