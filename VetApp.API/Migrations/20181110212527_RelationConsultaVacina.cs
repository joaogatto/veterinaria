﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VetApp.API.Migrations
{
    public partial class RelationConsultaVacina : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ConsultaId",
                table: "AnimaisVacinas",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AnimaisVacinas_ConsultaId",
                table: "AnimaisVacinas",
                column: "ConsultaId");

            migrationBuilder.AddForeignKey(
                name: "FK_AnimaisVacinas_Consultas_ConsultaId",
                table: "AnimaisVacinas",
                column: "ConsultaId",
                principalTable: "Consultas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AnimaisVacinas_Consultas_ConsultaId",
                table: "AnimaisVacinas");

            migrationBuilder.DropIndex(
                name: "IX_AnimaisVacinas_ConsultaId",
                table: "AnimaisVacinas");

            migrationBuilder.DropColumn(
                name: "ConsultaId",
                table: "AnimaisVacinas");
        }
    }
}
