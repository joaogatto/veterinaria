﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VetApp.API.Migrations
{
    public partial class AddedClinicasTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ClinicaId",
                table: "Funcionarios",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Role",
                table: "Funcionarios",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Clinicas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClinicaNome = table.Column<string>(nullable: true),
                    ClinicaLogin = table.Column<string>(nullable: true),
                    Responsavel = table.Column<string>(nullable: true),
                    Endereco = table.Column<string>(nullable: true),
                    SenhaHash = table.Column<byte[]>(nullable: true),
                    SenhaSalt = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clinicas", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Funcionarios_ClinicaId",
                table: "Funcionarios",
                column: "ClinicaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Funcionarios_Clinicas_ClinicaId",
                table: "Funcionarios",
                column: "ClinicaId",
                principalTable: "Clinicas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Funcionarios_Clinicas_ClinicaId",
                table: "Funcionarios");

            migrationBuilder.DropTable(
                name: "Clinicas");

            migrationBuilder.DropIndex(
                name: "IX_Funcionarios_ClinicaId",
                table: "Funcionarios");

            migrationBuilder.DropColumn(
                name: "ClinicaId",
                table: "Funcionarios");

            migrationBuilder.DropColumn(
                name: "Role",
                table: "Funcionarios");
        }
    }
}
