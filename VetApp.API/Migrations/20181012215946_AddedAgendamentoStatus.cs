﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VetApp.API.Migrations
{
    public partial class AddedAgendamentoStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Agendamentos_Funcionarios_ResponsavelId",
                table: "Agendamentos");

            migrationBuilder.DropIndex(
                name: "IX_Agendamentos_ResponsavelId",
                table: "Agendamentos");

            migrationBuilder.DropColumn(
                name: "ResponsavelId",
                table: "Agendamentos");

            migrationBuilder.CreateTable(
                name: "AgendamentoStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AgendamentoId = table.Column<int>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    QuemAlterouId = table.Column<int>(nullable: false),
                    QuemAlterouRole = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgendamentoStatus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AgendamentoStatus_Agendamentos_AgendamentoId",
                        column: x => x.AgendamentoId,
                        principalTable: "Agendamentos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Agendamentos_FuncionarioId",
                table: "Agendamentos",
                column: "FuncionarioId");

            migrationBuilder.CreateIndex(
                name: "IX_AgendamentoStatus_AgendamentoId",
                table: "AgendamentoStatus",
                column: "AgendamentoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Agendamentos_Funcionarios_FuncionarioId",
                table: "Agendamentos",
                column: "FuncionarioId",
                principalTable: "Funcionarios",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Agendamentos_Funcionarios_FuncionarioId",
                table: "Agendamentos");

            migrationBuilder.DropTable(
                name: "AgendamentoStatus");

            migrationBuilder.DropIndex(
                name: "IX_Agendamentos_FuncionarioId",
                table: "Agendamentos");

            migrationBuilder.AddColumn<int>(
                name: "ResponsavelId",
                table: "Agendamentos",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Agendamentos_ResponsavelId",
                table: "Agendamentos",
                column: "ResponsavelId");

            migrationBuilder.AddForeignKey(
                name: "FK_Agendamentos_Funcionarios_ResponsavelId",
                table: "Agendamentos",
                column: "ResponsavelId",
                principalTable: "Funcionarios",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
