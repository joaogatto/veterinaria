using System;
using System.Linq;
using AutoMapper;
using VetApp.API.Dtos;
using VetApp.API.Models;

namespace VetApp.API.Extensions
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            //front to back
            CreateMap<ClinicaCadastroDto, Clinica>();
            CreateMap<FuncionarioCadastroDto, Funcionario>();
            CreateMap<ClienteCadastroDto, Cliente>();
            CreateMap<ClienteListaDto, Cliente>().ReverseMap();
                // .ForMember(cli => cli.Animais, opt =>
                //     opt.MapFrom(cld => cld.Animais.Select(a => a.Id)));
            CreateMap<AnimalCadastroDto, Animal>();
            CreateMap<EspecieCadastroDto, Especie>();
            CreateMap<RacaCadastroDto, Raca>();
            CreateMap<AgendamentoFromClinicaDto, Agendamento>()
                .ForMember(a => a.DataHora, opt =>
                    opt.MapFrom(afc => new DateTime(1970,1,1).Add(TimeSpan.FromMilliseconds(afc.DataHora))));
            CreateMap<ConsultaFromCadastroDto, Consulta>()
                .ForMember(c => c.Sintomas, opt => opt.Ignore())
                .ForMember(c => c.Vacinas, opt => opt.Ignore())
                .ForMember(c => c.Exames, opt => opt.Ignore())
                .ForMember(c => c.Medicamentos, opt => opt.Ignore());
            CreateMap<ConsultaFromCadastroDto, AnimalPeso>()
                .ForMember(ap => ap.Id, opt =>
                    opt.Ignore())
                .ForMember(ap => ap.Data, opt =>
                    opt.UseValue(DateTime.Now));
            CreateMap<VacinaAplicacaoDto, AnimalVacina>();
            CreateMap<AgendamentoFinalizadoDto, Agendamento>();
            CreateMap<ExameFromClinicaDto, Exame>()
                .ForMember(e => e.DataHoraExame, opt =>
                    opt.UseValue(DateTime.Now));
            CreateMap<ExameFinalizarDto, Exame>();
            CreateMap<ExameVincularArquivoDto, Exame>();
            CreateMap<MedicamentosFromConsultaDto, Medicamento>()
                .ForMember(m => m.ConsultaId, opt => opt.Ignore());


            //back to front
            CreateMap<Cliente, ClienteListaDto>()
                .ForMember(cld => cld.Animais, opt => 
                    opt.MapFrom(cli => cli.Animais.Select(a => new KeyValuePair { Id = a.Id, Nome = a.Nome})));
            CreateMap<Especie, EspecieResource>()
                .ForMember(er => er.Racas, opt => 
                    opt.MapFrom(esp => esp.Racas.Select(r => new KeyValuePair { Id = r.Id, Nome = r.Nome })));
            CreateMap<Raca, RacaResource>()
                .ForMember(rs => rs.Especie, opt =>
                    opt.MapFrom(r => new KeyValuePair { Id = r.Especie.Id, Nome = r.Especie.Nome}))
                .ForMember(rs => rs.Animais, opt =>
                    opt.MapFrom(raca => raca.Animais.Select(a => new KeyValuePair{ Id = a.Id, Nome = a.Nome })));
            CreateMap<Animal, KeyValuePair>();
            CreateMap<Especie, KeyValuePair>();
            CreateMap<Raca, KeyValuePair>();
            CreateMap<Raca, RacaCadastroRetornoDto>();
            CreateMap<Funcionario, FuncionarioAgendamentoDto>();
            CreateMap<Agendamento, AgendamentoToListDto>()
                .ForMember(al => al.Animal, opt =>
                    opt.MapFrom(a => new KeyValuePair { Id = a.AnimalId, Nome = a.Animal.Nome }))
                .ForMember(al => al.Cliente, opt =>
                    opt.MapFrom(a => new KeyValuePair { Id = a.ClienteId, Nome = a.Animal.Cliente.Nome }))
                .ForMember(al => al.Funcionario, opt =>
                    opt.MapFrom(a => a.FuncionarioId.HasValue ?
                        new KeyValuePair { Id = a.Funcionario.Id, Nome = a.Funcionario.Nome } : null))
                .ForMember(al => al.StatusId, opt =>
                    opt.MapFrom(a => 0));
            CreateMap<Agendamento, AgendamentoFromClinicaDto>();
            CreateMap<Agendamento, AgendamentosDoFuncionarioDto>()
                .ForMember(af => af.Status, opt =>
                    opt.MapFrom(a => a.AgendamentoStatus.Count > 0 ?
                        new KeyValuePair { Id = a.AgendamentoStatus.Last().StatusId, Nome = a.AgendamentoStatus.Last().MotivoAlteracao} : null ));
            CreateMap<Agendamento, AgendamentoToConsultaDto>()
                .ForMember(ac => ac.Cliente, opt =>
                    opt.MapFrom(a => new KeyValuePair { Id = a.ClienteId, Nome = a.Cliente.Nome }))
                .ForMember(ac => ac.Animal, opt =>
                    opt.MapFrom(a => new KeyValuePair { Id = a.AnimalId, Nome = a.Animal.Nome }))
                .ForMember(ac => ac.RacaAnimal, opt =>
                    opt.MapFrom(a => new KeyValuePair { Id = a.Animal.RacaId, Nome = a.Animal.Raca.Nome}))
                .ForMember(ac => ac.DataNascAnimal, opt =>
                    opt.MapFrom(a => a.Animal.DataNascimento))
                .ForMember(ac => ac.Observacoes, opt =>
                    opt.MapFrom(a => a.Observacoes));
            CreateMap<Animal, AgendamentoToConsultaDto>()
                .ForMember(ac => ac.Id, opt =>
                    opt.UseValue(0))
                .ForMember(ac => ac.Animal, opt =>
                    opt.MapFrom(a => new KeyValuePair { Id = a.Id, Nome = a.Nome }))
                .ForMember(ac => ac.Cliente, opt =>
                    opt.MapFrom(a => new KeyValuePair { Id = a.Cliente.Id, Nome=  a.Cliente.Nome }))
                .ForMember(ac => ac.RacaAnimal, opt =>
                    opt.MapFrom(a => new KeyValuePair { Id = a.Raca.Id, Nome = a.Raca.Nome }))
                .ForMember(ac => ac.DataNascAnimal, opt =>
                    opt.MapFrom(a => a.DataNascimento))
                .ForMember(ac => ac.Observacoes, opt =>
                    opt.UseValue("Consulta rápida, sem agendamento."));
            //isso abaixo Id = id do AnimalVacina e não da vacina - Nome = Nome da vacina
            CreateMap<AnimalVacina, VacinaToConsultaDto>()
                .ForMember(kvp => kvp.NomeVacina, opt =>
                    opt.MapFrom(av => av.Vacina.Nome));
            CreateMap<Exame, KeyValuePair>();
            CreateMap<AnimalVacina, AniVacToVinculoDto>()
                .ForMember(avv => avv.Animal, opt =>
                    opt.MapFrom(av => new KeyValuePair { Id = av.AnimalId, Nome = av.Animal.Nome }))
                .ForMember(avv => avv.Vacina, opt =>
                    opt.MapFrom(av => new KeyValuePair { Id = av.Id, Nome = av.Vacina.Nome }));
            // Mapper.AssertConfigurationIsValid();
        }
    }
}