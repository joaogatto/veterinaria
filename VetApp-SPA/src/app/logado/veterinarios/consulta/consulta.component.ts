import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';
import { MatDialogRef, MatDialog, MatDialogConfig, MatChipInputEvent, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { AddSintomaComponent } from '../vetDialogs/add-sintoma/add-sintoma.component';
import { FuncionariosService } from '../../../_services/funcionarios.service';
import * as moment from 'moment';
import { KeyValuePair } from '../../../_models/KeyValuePair';
import { AgendamentoToConsulta } from '../../../_models/AgendamentoToConsulta';
import { SpinnerComponent } from '../../spinner/spinner.component';
import { ClienteLista } from '../../../_models/ClienteLista';
import { SelecionaClienteComponent } from '../../funcionarios/dialogs/seleciona-cliente/seleciona-cliente.component';
import { SelecionaAnimalComponent } from '../../funcionarios/dialogs/seleciona-animal/seleciona-animal.component';
import { AplicarVacinaComponent } from '../vetDialogs/aplicar-vacina/aplicar-vacina.component';
import { VacinaReturn } from '../../../_models/VacinaReturn';
import { ExameToConsulta } from '../../../_models/ExameToConsulta';
import { NovoExameComponent } from '../vetDialogs/novo-exame/novo-exame.component';
import { AddMedicamentoComponent } from '../vetDialogs/add-medicamento/add-medicamento.component';
import { MedicamentoToConsulta } from '../../../_models/MedicamentoToConsulta';

@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit {
  consultaForm = this.fb.group({
    AgendamentoId: ['0', Validators.required],
    AnimalId: ['0', Validators.required],
    DataHoraConsulta: [new Date(Date.now()), Validators.required],
    QueixaInicial: [''],
    Temperatura: [''],
    Peso: [''],
    Sintomas: this.fb.array([]),
    ExameClinico: [''],
    Diagnostico: [''],
    Vacinas: this.fb.array([]),
    Exames: this.fb.array([]),
    Medicamentos: this.fb.array([])
  });
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  sintomas: string[] = [];
  sintomasArray: FormArray;
  vacinasArray: FormArray;
  examesArray: FormArray;
  medicamentosArray: FormArray;
  addSintomaRef: MatDialogRef<AddSintomaComponent>;
  agendamento = {} as AgendamentoToConsulta;
  IdadeAnosMeses = '';
  spinnerDialogRef: MatDialogRef<SpinnerComponent>;
  cliDialogRef: MatDialogRef<SelecionaClienteComponent>;
  animalDialogRef: MatDialogRef<SelecionaAnimalComponent>;
  aplicarVacinaDialogRef: MatDialogRef<AplicarVacinaComponent>;
  novoExameDialogRef: MatDialogRef<NovoExameComponent>;
  addMedicamentoExameDialogRef: MatDialogRef<AddMedicamentoComponent>;
  cliente: ClienteLista;
  clientes: ClienteLista[];
  clienteId = 0;
  animalId = 0;
  vacinasAplicadas: VacinaReturn[] = [];
  examesRealizados: ExameToConsulta[] = [];
  medicamentosDaReceita: MedicamentoToConsulta[] = [];

  constructor(private route: ActivatedRoute,
    private fb: FormBuilder,
    private dialog: MatDialog,
    private funcService: FuncionariosService,
    private router: Router,
    private alerta: MatSnackBar) { }

  ngOnInit() {
    if (this.route.snapshot.params.agendamentoId) {
      this.funcService.getAgendamentoToConsulta(this.route.snapshot.params.agendamentoId)
        .subscribe(res => {
          this.agendamento = res;
          this.consultaForm.patchValue({
            AgendamentoId: this.agendamento.id,
            AnimalId: this.agendamento.animal.id
          });
          this.clienteId = this.agendamento.cliente.id;
          this.calculaIdade();
        }, error => console.log(error));
    } else {
      this.consultaSemAgendamento();
    }
  }

  calculaIdade() {
    let meses = moment().diff(this.agendamento.dataNascAnimal, 'months');
          let anos = 0;
          while (meses >= 12) {
            anos++;
            meses = meses - 12;
          }
          this.IdadeAnosMeses = anos + 'a, ' + meses + 'm';
  }

  consultaSemAgendamento() {
    this.funcService.getClientes().subscribe(res => {
      const dialogConfig = new MatDialogConfig();
      this.clientes = res;
      dialogConfig.data = res;
      dialogConfig.autoFocus = true;
      dialogConfig.disableClose = true;
      dialogConfig.id = 'selecionaCliente';
      dialogConfig.width = '600px';

    this.cliDialogRef = this.dialog.open(SelecionaClienteComponent, dialogConfig);

    this.cliDialogRef.afterClosed().subscribe(cli => {
      if (cli) {
      this.clienteId = cli.id;
      const cliente: ClienteLista = this.clientes.find(c => c.id === cli.id);
      this.clientes = [];
      this.clientes.push(cliente);

      const animalDialogConfig = new MatDialogConfig();
      animalDialogConfig.data = cliente;
      animalDialogConfig.autoFocus = true;
      animalDialogConfig.disableClose = true;
      animalDialogConfig.id = 'selecionaAnimal';
      animalDialogConfig.width = '600px';

      this.animalDialogRef = this.dialog.open(SelecionaAnimalComponent, animalDialogConfig);

      this.animalDialogRef.afterClosed().subscribe(animal => {
        if (animal) {
          this.funcService.getAnimalToConsulta(animal.id)
            .subscribe(response => {
              this.agendamento = response;
              this.consultaForm.patchValue({
                AgendamentoId: this.agendamento.id,
                AnimalId: this.agendamento.animal.id
              });
              this.calculaIdade();
            }, error => console.log(error));
        }
      });

      }
    });
    }, error => console.log('não foi possível obter clientes ou animais'));

    console.log(this.clienteId, this.animalId);
  }

  createSintoma(sintoma): FormGroup {
    return this.fb.group({
      descricao: sintoma
    });
  }

  createVacina(vacina: KeyValuePair): FormGroup {
    return this.fb.group({
      id: vacina.id,
      nome: vacina.nome
    });
  }

  addVacina(vacina: KeyValuePair) {
    this.vacinasArray = this.consultaForm.get('Vacinas') as FormArray;
    this.vacinasArray.push(this.createVacina(vacina));
  }

  createMedicamento(medicamento: MedicamentoToConsulta): FormGroup {
    return this.fb.group({
      nome: medicamento.nome,
      quantidade: medicamento.quantidade,
      unidadeDeMedida: medicamento.unidadeDeMedida,
      comoSerTomado: medicamento.comoSerTomado
    });
  }

  addMedicamento(medicamento: MedicamentoToConsulta) {
    this.medicamentosArray = this.consultaForm.get('Medicamentos') as FormArray;
    this.medicamentosArray.push(this.createMedicamento(medicamento));
  }

  createExame(exame: KeyValuePair): FormGroup {
    return this.fb.group({
      id: exame.id,
      nome: exame.nome
    });
  }

  addExame(exame: KeyValuePair) {
    this.examesArray = this.consultaForm.get('Exames') as FormArray;
    this.examesArray.push(this.createExame(exame));
  }

  addSintoma(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      if (this.sintomas.find(s => s === value.trim())) {
        if (input) {
          input.value = '';
        }
        return;
      }
      this.sintomas.push(value.trim());
      this.sintomasArray = this.consultaForm.get('Sintomas') as FormArray;
      this.sintomasArray.push(this.createSintoma(value.trim()));
    }

    if (input) {
      input.value = '';
    }
  }

  removeSintoma(sintoma): void {
    const index = this.sintomas.indexOf(sintoma);
    this.sintomasArray = this.consultaForm.get('Sintomas') as FormArray;
    this.sintomasArray.removeAt(this.sintomasArray.value.findIndex(sint => sint.descricao === sintoma));

    if (index >= 0) {
      this.sintomas.splice(index, 1);
    }
  }

  aplicarVacina() {
    this.funcService.getCliente(this.clienteId)
      .subscribe(res => {
        this.cliente = res;
      }, error => {
        this.alerta.open('Erro ao obter informações.', '', {
          panelClass: 'error-snackbar',
          duration: 5000
        });
      }, () => {
        const novoArrayAnimal: KeyValuePair[] = this.cliente.animais.filter(a => a.id === this.consultaForm.get('AnimalId').value);
        this.cliente.animais = novoArrayAnimal;
        const novoCliArray: ClienteLista[] = [];
        novoCliArray.push(this.cliente);
        const dialogConfig = new MatDialogConfig();
        dialogConfig.data = novoCliArray;
        dialogConfig.autoFocus = true;
        dialogConfig.id = 'aplicarVacinaConsulta';
        dialogConfig.width = '600px';
        dialogConfig.height = '500px';

        this.aplicarVacinaDialogRef = this.dialog.open(AplicarVacinaComponent, dialogConfig);

        this.aplicarVacinaDialogRef.afterClosed()
          .subscribe(res => {
            const vacinaAplicada: VacinaReturn = res;
            this.vacinasAplicadas.push(vacinaAplicada);
            const vacinaKvp: KeyValuePair = {
              id: vacinaAplicada.id,
              nome: vacinaAplicada.nomeVacina
            };
            this.addVacina(vacinaKvp);
            this.dialog.closeAll();
          });
      });
  }

  novoExame() {
    this.funcService.getCliente(this.clienteId)
      .subscribe(res => {
        this.cliente = res;
      }, error => {
        this.alerta.open('Erro ao obter informações.', '', {
          panelClass: 'error-snackbar',
          duration: 5000
        });
      }, () => {
        const novoArrayAnimal: KeyValuePair[] = this.cliente.animais.filter(a => a.id === this.consultaForm.get('AnimalId').value);
        this.cliente.animais = novoArrayAnimal;
        const novoCliArray: ClienteLista[] = [];
        novoCliArray.push(this.cliente);
        const dialogConfig = new MatDialogConfig();
          dialogConfig.data = novoCliArray;
          dialogConfig.autoFocus = true;
          dialogConfig.id = 'novoExame';
          dialogConfig.width = '600px';
          dialogConfig.height = '500px';

          this.novoExameDialogRef = this.dialog.open(NovoExameComponent, dialogConfig);

          this.novoExameDialogRef.afterClosed()
            .subscribe(res => {
              if (res) {
                const exameRealizado: ExameToConsulta = res;
                this.examesRealizados.push(exameRealizado);
                const exameKvp: KeyValuePair = {
                  id: exameRealizado.id,
                  nome: exameRealizado.nome
                };
                this.addExame(exameRealizado);
              }
            }, error => console.log(error));
      });

  }

  adicionarMedicamento() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.id = 'addMedicamento';
    dialogConfig.width = '600px';
    dialogConfig.height = '450px';

    this.addMedicamentoExameDialogRef = this.dialog.open(AddMedicamentoComponent, dialogConfig);

    this.addMedicamentoExameDialogRef.afterClosed()
      .subscribe(res => {
        if (res) {
          const novoMed: MedicamentoToConsulta = res;
          this.medicamentosDaReceita.push(novoMed);
          this.addMedicamento(novoMed);
        }
      });
  }

  finalizarConsulta() {
    console.log(this.consultaForm.value);
    this.spinnerDialogRef = this.dialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: true
    });
    this.funcService.cadastrarConsulta(this.consultaForm.value)
      .subscribe(res => {
        this.alerta.open('Consulta cadastrada com sucesso', '', {
          panelClass: 'success-snackbar',
          duration: 5000
        });
      }, error => {
        this.alerta.open('Erro ao cadastrar Consulta', '', {
          panelClass: 'error-snackbar',
          duration: 5000
        });
        this.spinnerDialogRef.close();
        console.log(error);
      }, () => {
        this.dialog.closeAll();
        this.router.navigate(['/funcionarios/home']);
      });
  }

}
