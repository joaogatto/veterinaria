import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Agendamento } from '../../../_models/Agendamento';
import { FuncionariosService } from '../../../_services/funcionarios.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-minhas-consultas',
  templateUrl: './minhas-consultas.component.html',
  styleUrls: ['./minhas-consultas.component.css']
})
export class MinhasConsultasComponent implements OnInit {
  agendamentos: Agendamento[] = [];
  agendamentosToShow: Agendamento[] = [];
  clienteTermo = '';
  statusTermo = 0;

  constructor(private dialog: MatDialog,
    private funcService: FuncionariosService,
    private router: Router) { }

  ngOnInit() {
    this.dialog.closeAll();
    this.funcService.getAgendamentosFunc()
      .subscribe(res => this.agendamentosToShow = res);

    setTimeout(() => {
      this.agendamentos = this.agendamentosToShow;
    }, 500);
  }

  consultasEmAberto(agendamentos: Agendamento[]) {
    const consultasEmAberto: Agendamento[] = [];

    agendamentos.forEach(agendamento => {
      if (agendamento.statusId === 1) {
        consultasEmAberto.push(agendamento);
      }
    });

    return consultasEmAberto;
  }

  todasAsConsultas() {
    return this.agendamentos;
  }

  consultasFinalizadas(agendamentos: Agendamento[]) {
    const consultasFinalizadas: Agendamento[] = [];

    agendamentos.forEach(agendamento => {
      if (agendamento.statusId === 3) {
        consultasFinalizadas.push(agendamento);
      }
    });

    return consultasFinalizadas;
  }

  termoClienteChanged(cliTermo) {
    this.clienteTermo = cliTermo;

    this.filtroAgendamentos(this.statusTermo, this.agendamentos, this.clienteTermo);
  }

  filtroAgendamentos(statusTermo, agendamentos: Agendamento[], clienteTermo) {
    let agendamentosReturn: Agendamento[] = [];
    const agendamentosFiltroAberto: Agendamento[] = [];
    const agendamentosFiltroFinalizados: Agendamento[] = [];
    this.statusTermo = statusTermo;

    if (clienteTermo !== undefined) {
      agendamentos.forEach(agendamento => {
        if (agendamento.cliente.nome.toLowerCase().includes(clienteTermo.toLowerCase())) {
          agendamentosReturn.push(agendamento);
        }
      });
    } else {
      agendamentosReturn = agendamentos;
    }

    if (this.statusTermo === 1) {
      agendamentosReturn = this.consultasEmAberto(agendamentosReturn);
    } else if (this.statusTermo === 3) {
      agendamentosReturn = this.consultasFinalizadas(agendamentosReturn);
    }

    this.agendamentosToShow = agendamentosReturn;
    console.log('toaqui');
  }
}
