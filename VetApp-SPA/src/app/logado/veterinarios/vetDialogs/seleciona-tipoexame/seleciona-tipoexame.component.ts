import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';

@Component({
  selector: 'app-seleciona-tipoexame',
  templateUrl: './seleciona-tipoexame.component.html',
  styleUrls: ['./seleciona-tipoexame.component.css']
})
export class SelecionaTipoexameComponent implements OnInit {
  selecionaTipoExame: MatDialogRef<SelecionaTipoexameComponent>;

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  pedido() {
    this.selecionaTipoExame = this.dialog.getDialogById('selecionaTipoexame');
    console.log(this.dialog.getDialogById('selecionaTipoexame'));
    this.selecionaTipoExame.close(1);
  }

  exame() {
    this.selecionaTipoExame = this.dialog.getDialogById('selecionaTipoexame');
    this.selecionaTipoExame.close(3);
  }
}
