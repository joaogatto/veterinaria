import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MatDialog } from '@angular/material';
import { MedicamentoToConsulta } from '../../../../_models/MedicamentoToConsulta';

@Component({
  selector: 'app-add-medicamento',
  templateUrl: './add-medicamento.component.html',
  styleUrls: ['./add-medicamento.component.css']
})
export class AddMedicamentoComponent implements OnInit {
  addMedicamentoForm = this.fb.group({
   Nome: ['', Validators.required],
   Quantidade: ['', Validators.required],
   UnidadeDeMedida: ['', Validators.required],
   ComoSerTomado: ['', Validators.required]
  });
  addMedicamentoDialogRef: MatDialogRef<AddMedicamentoComponent>;

  constructor(private fb: FormBuilder, private dialog: MatDialog) { }

  ngOnInit() {
  }

  adicionarMedicamento() {
    this.addMedicamentoDialogRef = this.dialog.getDialogById('addMedicamento');

    const novoMed: MedicamentoToConsulta = {
      nome: this.addMedicamentoForm.get('Nome').value,
      quantidade: this.addMedicamentoForm.get('Quantidade').value,
      unidadeDeMedida: this.addMedicamentoForm.get('UnidadeDeMedida').value,
      comoSerTomado: this.addMedicamentoForm.get('ComoSerTomado').value,
    };

    this.addMedicamentoDialogRef.close(novoMed);
  }
}
