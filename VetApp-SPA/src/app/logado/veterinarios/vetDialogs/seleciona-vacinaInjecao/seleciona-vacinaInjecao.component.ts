import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatSelectionList, MatDialogRef, MatDialog } from '@angular/material';
import { KeyValuePair } from '../../../../_models/KeyValuePair';
import { FuncionariosService } from '../../../../_services/funcionarios.service';
import { VacinaToVinculo } from '../../../../_models/VacinaToVinculo';
import { VacinaReturn } from '../../../../_models/VacinaReturn';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-seleciona-vacinaInjecao',
  templateUrl: './seleciona-vacinaInjecao.component.html',
  styleUrls: ['./seleciona-vacinaInjecao.component.css']
})
export class SelecionaVacinaInjecaoComponent implements OnInit {
  animal: KeyValuePair;
  @ViewChild(MatSelectionList) selectionList: MatSelectionList;
  vacinaSelecionada: number;
  vacinasDoAnimal: VacinaToVinculo[] = [];
  selectVacinaInjecaoDialogRef: MatDialogRef<SelecionaVacinaInjecaoComponent>;
  vacinaReturn: VacinaReturn;

  constructor(@Inject(MAT_DIALOG_DATA) public data,
    private funcService: FuncionariosService,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.funcService.getVacInjSemConsulta(this.data.id)
      .subscribe(res => {
        this.vacinasDoAnimal = res;
      });

    this.animal = this.data;
  }

  vacinaMudou(vacinaId) {
    this.vacinaSelecionada = vacinaId;
    const vacina: VacinaToVinculo = this.vacinasDoAnimal.find(vac => vac.id === vacinaId);

    this.vacinaReturn = {
      id: vacina.id,
      nomeVacina: vacina.vacina.nome,
      quantidadeAplicada: vacina.quantidadeAplicada,
      dataHoraAplicacao: vacina.dataHoraAplicacao,
      animal: this.animal;
    };

    this.selectVacinaInjecaoDialogRef = this.dialog.getDialogById('selecionaVacInj');

    this.selectVacinaInjecaoDialogRef.close(this.vacinaReturn);
  }

}
