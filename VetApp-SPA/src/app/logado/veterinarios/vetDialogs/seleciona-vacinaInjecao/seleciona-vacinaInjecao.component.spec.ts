/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { SelecionaVacinaInjecaoComponent } from './seleciona-vacinaInjecao.component';

describe('SelecionaVacinaInjecaoComponent', () => {
  let component: SelecionaVacinaInjecaoComponent;
  let fixture: ComponentFixture<SelecionaVacinaInjecaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelecionaVacinaInjecaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelecionaVacinaInjecaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
