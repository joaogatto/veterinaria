import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogConfig, MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { ClienteLista } from '../../../../_models/ClienteLista';
import { FuncionariosService } from '../../../../_services/funcionarios.service';
import { SelecionaClienteComponent } from '../../../funcionarios/dialogs/seleciona-cliente/seleciona-cliente.component';
import { SelecionaAnimalComponent } from '../../../funcionarios/dialogs/seleciona-animal/seleciona-animal.component';
import { FormBuilder, Validators } from '@angular/forms';
import { KeyValuePair } from '../../../../_models/KeyValuePair';
import { Router } from '@angular/router';
import { SpinnerComponent } from '../../../spinner/spinner.component';
import { VacinaReturn } from '../../../../_models/VacinaReturn';

@Component({
  selector: 'app-aplicar-vacina',
  templateUrl: './aplicar-vacina.component.html',
  styleUrls: ['./aplicar-vacina.component.css']
})
export class AplicarVacinaComponent implements OnInit {
  aplicaVacinaForm = this.fb.group({
    AnimalId: ['', Validators.required],
    VacinaId: ['', Validators.required],
    QuantidadeAplicada: ['', Validators.required],
    DataHoraAplicacao: [new Date(Date.now()), Validators.required]
  });
  clientes: ClienteLista[] = [];
  cliente: KeyValuePair = {
    id: 0,
    nome: ''
  };
  animal: KeyValuePair = {
    id: 0,
    nome: ''
  };
  cliDialogRef: MatDialogRef<SelecionaClienteComponent>;
  animalDialogRef: MatDialogRef<SelecionaAnimalComponent>;
  spinnerDialogRef: MatDialogRef<SpinnerComponent>;
  aplicarVacinaDialogRef: MatDialogRef<AplicarVacinaComponent>;
  vacinas: KeyValuePair[] = [];

  constructor(@Inject(MAT_DIALOG_DATA) public data: ClienteLista[],
    private funcService: FuncionariosService,
    private dialog: MatDialog,
    private fb: FormBuilder,
    private alerta: MatSnackBar,
    private router: Router) { }

  ngOnInit() {
    this.funcService.getVacinas()
      .subscribe(res => {
        this.vacinas = res;
      }, error => console.log(error));

      setTimeout(() => {
        this.setCliAnimal();
      }, 500);
  }

  setCliAnimal() {

    this.clientes = this.data;
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = this.clientes;
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.id = 'selecionaCliente';
    dialogConfig.width = '600px';

    this.cliDialogRef = this.dialog.open(SelecionaClienteComponent, dialogConfig);

    this.cliDialogRef.afterClosed().subscribe(cli => {
      if (cli) {
      const cliente: ClienteLista = this.clientes.find(c => c.id === cli.id);

      const animalDialogConfig = new MatDialogConfig();
      animalDialogConfig.data = cliente;
      animalDialogConfig.autoFocus = true;
      animalDialogConfig.disableClose = true;
      animalDialogConfig.id = 'selecionaAnimal';
      animalDialogConfig.width = '600px';

      this.animalDialogRef = this.dialog.open(SelecionaAnimalComponent, animalDialogConfig);

      this.animalDialogRef.afterClosed().subscribe(animal => {
        if (animal) {
          this.aplicaVacinaForm.patchValue({
            AnimalId: animal.id
          });
          this.animal = {
            id: animal.id,
            nome: animal.nome
          };
          this.cliente = {
            id: cliente.id,
            nome: cliente.nome
          };
        }
      });

      }
    });
  }

  cadastrarAplicacao() {
    this.spinnerDialogRef = this.dialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: true
    });
    this.funcService.aplicarVacina(this.aplicaVacinaForm.value)
      .subscribe(res => {
        this.alerta.open('Aplicação realizada com sucesso!', '', {
          panelClass: 'success-snackbar',
          duration: 5000
        });
        const vacinaAplicada = res;
        this.aplicarVacinaDialogRef = this.dialog.getDialogById('aplicarVacinaConsulta');
        if (this.aplicarVacinaDialogRef !== undefined) {
          this.aplicarVacinaDialogRef.close(vacinaAplicada);
        }
      }, error => {
        this.alerta.open(error.error, '', {
          panelClass: 'error-snackbar',
          duration: 5000
        });
        this.spinnerDialogRef.close();
      }, () => {
        if (this.clientes.length > 1) {
          this.router.navigate(['/funcionarios/home']);
        }
        if (this.aplicarVacinaDialogRef === undefined) {
          this.dialog.closeAll();
        }
      });
  }

}
