import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-add-sintoma',
  templateUrl: './add-sintoma.component.html',
  styleUrls: ['./add-sintoma.component.css']
})
export class AddSintomaComponent implements OnInit {
  descricaoSintoma: any;

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  adicionarSintoma() {
    this.dialog.getDialogById('adicionarSintoma').close(this.descricaoSintoma);
  }

}
