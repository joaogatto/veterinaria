import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogConfig, MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { ClienteLista } from '../../../../_models/ClienteLista';
import { SelecionaClienteComponent } from '../../../funcionarios/dialogs/seleciona-cliente/seleciona-cliente.component';
import { SelecionaAnimalComponent } from '../../../funcionarios/dialogs/seleciona-animal/seleciona-animal.component';
import { FormBuilder, Validators } from '@angular/forms';
import { KeyValuePair } from '../../../../_models/KeyValuePair';
import { SelecionaTipoexameComponent } from '../seleciona-tipoexame/seleciona-tipoexame.component';
import { SpinnerComponent } from '../../../spinner/spinner.component';
import { FuncionariosService } from '../../../../_services/funcionarios.service';
import { Router } from '@angular/router';
import { ExameToConsulta } from '../../../../_models/ExameToConsulta';

@Component({
  selector: 'app-novo-exame',
  templateUrl: './novo-exame.component.html',
  styleUrls: ['./novo-exame.component.css']
})
export class NovoExameComponent implements OnInit {
  novoExameForm = this.fb.group({
    Nome: ['', Validators.required],
    AnimalId: ['', Validators.required],
    Status: [3, Validators.required],
    Resultado: ['']
  });
  cliDialogRef: MatDialogRef<SelecionaClienteComponent>;
  animalDialogRef: MatDialogRef<SelecionaAnimalComponent>;
  tipoExameDialogRef: MatDialogRef<SelecionaTipoexameComponent>;
  novoExameDialogRef: MatDialogRef<NovoExameComponent>;
  spinnerDialogRef: MatDialogRef<SpinnerComponent>;
  clientes: ClienteLista[] = [];
  cliente: KeyValuePair = {
    id: 0,
    nome: ''
  };
  animal: KeyValuePair = {
    id: 0,
    nome: ''
  };
  arquivo: File;
  exameReturn: ExameToConsulta = {
    id: 0,
    nome: '',
    ehPedido: false
  };

  constructor(@Inject(MAT_DIALOG_DATA) public data: ClienteLista[],
    private fb: FormBuilder,
    private dialog: MatDialog,
    private alerta: MatSnackBar,
    private funcService: FuncionariosService,
    private router: Router) { }

  ngOnInit() {
    setTimeout(() => {
      this.setCliAnimalTipo();
    }, 500);
  }

  setCliAnimalTipo() {

    this.clientes = this.data;

    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = this.data;
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.id = 'selecionaCliente';
    dialogConfig.width = '600px';

    this.cliDialogRef = this.dialog.open(SelecionaClienteComponent, dialogConfig);

    this.cliDialogRef.afterClosed().subscribe(cli => {
      if (cli) {
      const cliente: ClienteLista = this.clientes.find(c => c.id === cli.id);

      const animalDialogConfig = new MatDialogConfig();
      animalDialogConfig.data = cliente;
      animalDialogConfig.autoFocus = true;
      animalDialogConfig.disableClose = true;
      animalDialogConfig.id = 'selecionaAnimal';
      animalDialogConfig.width = '600px';
    console.log(animalDialogConfig);

      this.animalDialogRef = this.dialog.open(SelecionaAnimalComponent, animalDialogConfig);

      this.animalDialogRef.afterClosed().subscribe(animal => {
        if (animal) {
          this.novoExameForm.patchValue({
            AnimalId: animal.id
          });
          this.animal = {
            id: animal.id,
            nome: animal.nome
          };
          this.cliente = {
            id: cliente.id,
            nome: cliente.nome
          };

          const tipoeExameDialogConfig = new MatDialogConfig();
          tipoeExameDialogConfig.autoFocus = true;
          tipoeExameDialogConfig.disableClose = true;
          tipoeExameDialogConfig.id = 'selecionaTipoexame';
          tipoeExameDialogConfig.width = '500px';

          this.tipoExameDialogRef = this.dialog.open(SelecionaTipoexameComponent, tipoeExameDialogConfig);

          this.tipoExameDialogRef.afterClosed().subscribe(tipo => {
            if (tipo) {
              this.novoExameForm.patchValue({
                Status: tipo
              });
            }
          });
        }
      });

      }
    });
  }

  inserirArquivo(event) {
    if (event !== undefined) {
      if (event.target.files.length > 1) {
        this.alerta.open('Não é possível inserir mais de uma imagem.', '', {
            duration: 3000,
            panelClass: 'error-snackbar'
        });

        return 0;
      }

      this.arquivo = event.target.files[0];

      console.log(this.arquivo);

      if (this.arquivo.type !== 'image/jpeg' && this.arquivo.type !== 'application/pdf') {
        this.alerta.open('Formato de arquivo incorreto. Só aceitamos PDF/JPEG/JPG', '', {
          duration: 3000,
          panelClass: 'error-snackbar'
      });

      this.arquivo = undefined;
      return 0;
      }

      const tipo = this.arquivo.type;
      if (this.arquivo.type === 'image/jpeg' && this.arquivo.size / 1000 > 1000) {
        this.alerta.open('Tamanho máximo excedido. Tamanho máximo = 1mb', '', {
          duration: 3000,
          panelClass: 'error-snackbar'
        });

        this.arquivo = undefined;
        return 0;
      }

      if (tipo === 'application/pdf' && this.arquivo.size / 1000 > 3000) {
        this.alerta.open('Tamanho máximo excedido. Tamanho máximo PDF = 3mb', '', {
          duration: 3000,
          panelClass: 'error-snackbar'
        });

      this.arquivo = undefined;
      return 0;
      }
    }

    return 0;
  }

  cadastrarExame() {
    const status = this.novoExameForm.get('Status').value;
    const resultado = this.novoExameForm.get('Resultado').value;
    if (status === 3 && this.arquivo === undefined && resultado.length < 1) {
      this.alerta.open('Você precisa Adicionar um Arquivo, registrar um Resultado ou realizar um Pedido de Exame', '', {
        duration: 5000,
        panelClass: 'info-snackbar'
      });

      return 0;
    }

    this.spinnerDialogRef = this.dialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: true
    });

    this.funcService.cadastrarExame(this.novoExameForm.value)
      .subscribe(res => {
        this.alerta.open('Exame cadastrado com sucesso!', '', {
          panelClass: 'success-snackbar',
          duration: 5000
        });
        this.novoExameDialogRef = this.dialog.getDialogById('novoExame');

        if (status === 3 && this.arquivo !== undefined) {
          this.funcService.vincularArquivoExame(res.id, this.arquivo)
            .subscribe(arquivores => 0, error => {
              this.alerta.open('Exame cadastrado porém ocorreram problemas com o arquivo', '', {
                duration: 5000,
                panelClass: 'info-snackbar'
              });
            });
          }

          this.exameReturn.id = res.id;
          this.exameReturn.nome = res.nome;

          if (status === 3) {
            this.exameReturn.ehPedido = false;
          } else {
            this.exameReturn.ehPedido = true;
          }

          this.novoExameDialogRef.close(this.exameReturn);
          this.spinnerDialogRef.close();
      }, error => {
        this.alerta.open('Exame cadastrado com sucesso!', '', {
          panelClass: 'error-snackbar',
          duration: 5000
        });
        console.log(error);
        this.spinnerDialogRef.close();
      });
  }

}
