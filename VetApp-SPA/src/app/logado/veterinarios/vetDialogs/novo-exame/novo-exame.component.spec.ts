/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { NovoExameComponent } from './novo-exame.component';

describe('NovoExameComponent', () => {
  let component: NovoExameComponent;
  let fixture: ComponentFixture<NovoExameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NovoExameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NovoExameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
