/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { VincularVacinaEexameComponent } from './vincular-vacina-eexame.component';

describe('VincularVacinaEexameComponent', () => {
  let component: VincularVacinaEexameComponent;
  let fixture: ComponentFixture<VincularVacinaEexameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VincularVacinaEexameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VincularVacinaEexameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
