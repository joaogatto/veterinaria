import { Component, OnInit } from '@angular/core';
import { FuncionariosService } from '../../../_services/funcionarios.service';
import { ClienteLista } from '../../../_models/ClienteLista';
import { MatDialogConfig, MatDialogRef, MatDialog } from '@angular/material';
import { SelecionaClienteComponent } from '../../funcionarios/dialogs/seleciona-cliente/seleciona-cliente.component';
import { SelecionaAnimalComponent } from '../../funcionarios/dialogs/seleciona-animal/seleciona-animal.component';
import { VincularVacInjExaComponent } from '../vetDialogs/vincular-vacInjExa/vincular-vacInjExa.component';
import { SelecionaConsultaComponent } from '../vetDialogs/seleciona-consulta/seleciona-consulta.component';
import { SelecionaVacinaInjecaoComponent } from '../vetDialogs/seleciona-vacinaInjecao/seleciona-vacinaInjecao.component';
import { VacinaReturn } from '../../../_models/VacinaReturn';
import { QueryVinculoVacinaExame } from '../../../_models/QueryVinculoVacinaExame';

@Component({
  selector: 'app-vincular-vacina-eexame',
  templateUrl: './vincular-vacina-eexame.component.html',
  styleUrls: ['./vincular-vacina-eexame.component.css']
})
export class VincularVacinaEexameComponent implements OnInit {
  clientes: ClienteLista[] = [];
  cliDialogRef: MatDialogRef<SelecionaClienteComponent>;
  animalDialogRef: MatDialogRef<SelecionaAnimalComponent>;
  vincularDialogRef: MatDialogRef<VincularVacInjExaComponent>;
  selecionaVacInjDialogRef: MatDialogRef<SelecionaVacinaInjecaoComponent>;
  selecionaConsultaDialogRef: MatDialogRef<SelecionaConsultaComponent>;
  vacinaToAdd: VacinaReturn;

  constructor(private funcService: FuncionariosService,
    private dialog: MatDialog) { }

  ngOnInit() {
  }

  vincularVacinaInjecao() {

    this.funcService.getClientes()
    .subscribe(res => {

      this.clientes = res;
      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = this.clientes;
      dialogConfig.autoFocus = true;
      dialogConfig.disableClose = true;
      dialogConfig.id = 'selecionaCliente';
      dialogConfig.width = '600px';

      this.cliDialogRef = this.dialog.open(SelecionaClienteComponent, dialogConfig);

      this.cliDialogRef.afterClosed().subscribe(cli => {
        if (cli) {
          const cliente: ClienteLista = this.clientes.find(c => c.id === cli.id);

          const animalDialogConfig = new MatDialogConfig();
          animalDialogConfig.data = cliente;
          animalDialogConfig.autoFocus = true;
          animalDialogConfig.disableClose = true;
          animalDialogConfig.id = 'selecionaAnimal';
          animalDialogConfig.width = '600px';

          this.animalDialogRef = this.dialog.open(SelecionaAnimalComponent, animalDialogConfig);

          this.animalDialogRef.afterClosed().subscribe(animal => {
            if (animal) {
              const selecionaVacInjDialogConfig = new MatDialogConfig();
              selecionaVacInjDialogConfig.data = animal;
              selecionaVacInjDialogConfig.autoFocus = true;
              selecionaVacInjDialogConfig.disableClose = false;
              selecionaVacInjDialogConfig.id = 'selecionaVacInj';
              selecionaVacInjDialogConfig.width = '600px';
              selecionaVacInjDialogConfig.minHeight = '200px';

              this.selecionaVacInjDialogRef = this.dialog.open(SelecionaVacinaInjecaoComponent, selecionaVacInjDialogConfig);

              this.selecionaVacInjDialogRef.afterClosed()
                .subscribe(vac => {
                  if (vac) {
                    this.vacinaToAdd = vac;

                    const consultaDialogData: QueryVinculoVacinaExame = {
                      animalId: this.vacinaToAdd.animal.id,
                      data: new Date(this.vacinaToAdd.dataHoraAplicacao)
                    };

                    const selecionaConsultaDialogConfig = new MatDialogConfig();
                    animalDialogConfig.data = consultaDialogData;
                    animalDialogConfig.autoFocus = true;
                    animalDialogConfig.disableClose = false;
                    animalDialogConfig.id = 'selecionaConsulta';
                    animalDialogConfig.width = '600px';

                    this.selecionaConsultaDialogRef = this.dialog.open(SelecionaConsultaComponent, selecionaConsultaDialogConfig);
                  }
                });


              // const vincDialogData = {
              //   animalId: animal.id,
              //   vinculoTipo: 'vacinaInjecao'
              // };
              // const vincDialogConfig = new MatDialogConfig();
              // animalDialogConfig.data = vincDialogData;
              // animalDialogConfig.autoFocus = true;
              // animalDialogConfig.disableClose = true;
              // animalDialogConfig.id = 'vinc';
              // animalDialogConfig.width = '600px';

              // this.vincularDialogRef = this.dialog.open(VincularVacInjExaComponent, vincDialogConfig);
            }
          });
        }
      });
    });
  }

}
