import { Component, OnInit } from '@angular/core';
import { SpinnerComponent } from '../../spinner/spinner.component';
import { MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { GerentesServiceService } from '../../../_services/gerentesService.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastrar-funcionario',
  templateUrl: './cadastrar-funcionario.component.html',
  styleUrls: ['./cadastrar-funcionario.component.css']
})
export class CadastrarFuncionarioComponent implements OnInit {
  cadFuncForm = this.fb.group({
    Nome: ['', Validators.required],
    Email: ['', [Validators.required, Validators.email]],
    Senha: ['', [Validators.required, Validators.minLength(4)]],
    Role: ['', Validators.required]
  });
  spinnerDialogRef: MatDialogRef<SpinnerComponent>;
  confirmarSenha = '';


  constructor(private dialog: MatDialog,
    private fb: FormBuilder,
    private gerenteService: GerentesServiceService,
    private alerta: MatSnackBar,
    private router: Router) { }

  ngOnInit() {
    this.dialog.closeAll();
  }

  inputConfirmarSenha(campo) {
    this.confirmarSenha = campo;
  }


  cadastrarFuncionario() {
    const spinDialogRef: MatDialogRef<SpinnerComponent> = this.dialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: true
    });
    const func = this.cadFuncForm.value;
    if (func.Senha === this.confirmarSenha) {
      this.gerenteService.cadastrarFuncionario(this.cadFuncForm.value)
        .subscribe(res => {
          this.alerta.open('Funcionário cadastrado com sucesso!', '', {
            duration: 5000,
            panelClass: 'success-snackbar'
          });
          this.router.navigate(['/funcionarios/home']);
        }, error => {
          this.alerta.open(error.error, '', {
            duration: 5000,
            panelClass: 'error-snackbar'
          });
          this.dialog.closeAll();
        }, () => this.dialog.closeAll());
    } else {
      this.dialog.closeAll();
      this.alerta.open('Senha e Confirmar Senha devem ser Iguais', '', {
        duration: 5000,
        panelClass: 'error-snackbar'
      });
    }
  }

}
