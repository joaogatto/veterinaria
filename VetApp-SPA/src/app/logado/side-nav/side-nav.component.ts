import {MediaMatcher} from '@angular/cdk/layout';
import { Component, OnInit, OnDestroy, ChangeDetectorRef, Input, Output, EventEmitter } from '@angular/core';
import { LoginService } from '../../_services/login.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenModel } from '../../_models/TokenModel';
import { Router } from '@angular/router';
import { SpinnerComponent } from '../spinner/spinner.component';
import { MatDialogRef, MatDialog } from '@angular/material';


@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit, OnDestroy {
  @Output() logado = new EventEmitter();
  token: TokenModel;
  spinner: MatDialogRef<SpinnerComponent>;

  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;

  ngOnInit() {
    if (this.loginService.logado()) {
      this.token = this.loginService.tokenDecript;
    }
  }

  constructor(changeDetectorRef: ChangeDetectorRef,
      media: MediaMatcher,
      private loginService: LoginService,
      private router: Router,
      private spinnerDialog: MatDialog) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  goHome() {
    if (this.token.role === 'Gerente' || this.token.role === 'Funcionario' || this.token.role === 'Veterinario') {
      this.router.navigate(['/funcionarios/home']);
    } else if (this.token.role === 'Admin') {
      this.router.navigate(['/admin/home']);
    }
  }

  deslogar() {
    if (this.loginService.deslogar()) {
      console.log('deslogarfromnav');
      this.logado.emit(false);
    }
  }

  carregando(url) {
    if (this.router.url === url) {
      return 0;
    }
    this.spinner = this.spinnerDialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: true
    });
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

}
