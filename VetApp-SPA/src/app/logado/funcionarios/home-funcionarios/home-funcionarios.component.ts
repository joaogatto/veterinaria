import { Component, OnInit } from '@angular/core';
import { FuncionariosService } from '../../../_services/funcionarios.service';
import { ClienteLista } from '../../../_models/ClienteLista';
import { MatDialog, MatDialogRef, MatDialogConfig, MatSnackBar } from '@angular/material';
import { CadastrarAnimalComponent } from '../animais/cadastrar-animal/cadastrar-animal.component';
import { SpinnerComponent } from '../../spinner/spinner.component';
import { CadastrarAgendamentoComponent } from '../dialogs/cadastrar-agendamento/cadastrar-agendamento.component';
import { Router } from '@angular/router';
import { AplicarVacinaComponent } from '../../veterinarios/vetDialogs/aplicar-vacina/aplicar-vacina.component';
import { TokenModel } from '../../../_models/TokenModel';
import { LoginService } from '../../../_services/login.service';
import { NovoExameComponent } from '../../veterinarios/vetDialogs/novo-exame/novo-exame.component';

@Component({
  selector: 'app-home-funcionarios',
  templateUrl: './home-funcionarios.component.html',
  styleUrls: ['./home-funcionarios.component.css']
})
export class HomeFuncionariosComponent implements OnInit {

  spinnerDialogRef: MatDialogRef<SpinnerComponent>;
  cadAnimalDialogRef: MatDialogRef<CadastrarAnimalComponent>;
  cadAgendamentoDialogRef: MatDialogRef<CadastrarAgendamentoComponent>;
  aplicarVacinaDialogRef: MatDialogRef<AplicarVacinaComponent>;
  novoExameDialogRef: MatDialogRef<NovoExameComponent>;
  token: TokenModel;

  constructor(private funcService: FuncionariosService,
    private dialog: MatDialog,
    private alerta: MatSnackBar,
    private router: Router,
    private loginService: LoginService) { }

  ngOnInit() {
    if (this.loginService.logado()) {
      this.token = this.loginService.tokenDecript;
    }
  }

  cadastrarAnimal() {
    this.spinnerDialogRef = this.dialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: true
    });
    let clientes: ClienteLista[];
    this.funcService.getClientes().subscribe(res => {
      clientes = res;
      this.dialog.closeAll();

      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = clientes;
      dialogConfig.autoFocus = true;
      dialogConfig.id = 'cadAnimal';
      dialogConfig.width = '600px';
      dialogConfig.height = '500px';

      this.cadAnimalDialogRef = this.dialog.open(CadastrarAnimalComponent, dialogConfig);
    }, error => {
      this.dialog.closeAll();
      this.alerta.open('Erro ao carregar clientes', '', {
        panelClass: 'error-snackbar',
        duration: 5000
      });
    });
  }

  cadastrarAgendamento() {
    this.spinnerDialogRef = this.dialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: true
    });
    let clientes: ClienteLista[];
    this.funcService.getClientes().subscribe(res => {
      clientes = res;
      this.dialog.closeAll();

      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = clientes;
      dialogConfig.autoFocus = true;
      dialogConfig.id = 'cadAgendamento';
      dialogConfig.width = '600px';
      dialogConfig.height = '400px';

      this.cadAgendamentoDialogRef = this.dialog.open(CadastrarAgendamentoComponent, dialogConfig);
    }, error => {
      this.dialog.closeAll();
      this.alerta.open('Erro ao carregar clientes', '', {
        panelClass: 'error-snackbar',
        duration: 5000
      });
    });
  }

  gerarConsulta() {
    this.router.navigate(['/veterinarios/gerarConsulta']);
  }

  aplicarVacina() {
    this.spinnerDialogRef = this.dialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: true
    });
    let clientes: ClienteLista[];
    this.funcService.getClientes().subscribe(res => {
      clientes = res;
      this.dialog.closeAll();

      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = clientes;
      dialogConfig.autoFocus = true;
      dialogConfig.id = 'aplicarVacina';
      dialogConfig.width = '600px';
      dialogConfig.height = '500px';

      this.aplicarVacinaDialogRef = this.dialog.open(AplicarVacinaComponent, dialogConfig);
    }, error => {
      this.dialog.closeAll();
      this.alerta.open('Erro ao carregar clientes', '', {
        panelClass: 'error-snackbar',
        duration: 5000
      });
    });
  }

  novoExame() {
    this.spinnerDialogRef = this.dialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: true
    });
    let clientes: ClienteLista[];
    this.funcService.getClientes().subscribe(res => {
      clientes = res;
      this.dialog.closeAll();

      const dialogConfig = new MatDialogConfig();
      dialogConfig.data = clientes;
      dialogConfig.autoFocus = true;
      dialogConfig.id = 'novoExame';
      dialogConfig.width = '600px';
      dialogConfig.height = '500px';

      this.novoExameDialogRef = this.dialog.open(NovoExameComponent, dialogConfig);
    }, error => {
      this.dialog.closeAll();
      this.alerta.open('Erro ao carregar clientes', '', {
        panelClass: 'error-snackbar',
        duration: 5000
      });
    });
  }

}
