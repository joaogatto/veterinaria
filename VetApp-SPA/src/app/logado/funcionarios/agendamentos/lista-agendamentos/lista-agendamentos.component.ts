import { Component, OnInit } from '@angular/core';
import { FuncionariosService } from '../../../../_services/funcionarios.service';
import { MatDialog, MatDialogRef, MatDialogConfig, MatSnackBar } from '@angular/material';
import { Agendamento } from '../../../../_models/Agendamento';
import { EditarAgendamentoComponent } from '../../dialogs/editar-agendamento/editar-agendamento.component';
import { CancelarAgendamentoComponent } from '../../dialogs/cancelar-agendamento/cancelar-agendamento.component';
import { Router } from '@angular/router';
import { CancelarAgendamento } from '../../../../_models/CancelarAgendamento';
import { FinalizarAgendamentoComponent } from '../../dialogs/finalizar-agendamento/finalizar-agendamento.component';

@Component({
  selector: 'app-lista-agendamentos',
  templateUrl: './lista-agendamentos.component.html',
  styleUrls: ['./lista-agendamentos.component.css']
})
export class ListaAgendamentosComponent implements OnInit {
  agendamentos: Agendamento[] = [];
  cancelarAgendamentoDialogRef: MatDialogRef<CancelarAgendamentoComponent>;
  editaAgendamentoDialogRef: MatDialogRef<EditarAgendamentoComponent>;
  finalizarAgendamentoDialogRef: MatDialogRef<FinalizarAgendamentoComponent>;

  constructor(private funcService: FuncionariosService,
    private dialog: MatDialog,
    private alerta: MatSnackBar,
    private router: Router) { }

  ngOnInit() {
    this.funcService.getAgendamentos()
      .subscribe(res => {
        this.agendamentos = res;
        console.log(res);
      }, error => this.dialog.closeAll(), () => this.dialog.closeAll());
  }

  editarAgendamento(id) {
    const agendamentoToEdit: Agendamento = this.agendamentos.find(a => a.id === id);

    if (agendamentoToEdit.statusId === 3 || agendamentoToEdit.statusId === 4) {
      this.naoEhPossivel();

      return 0;
    }

    const dialogConfig = new MatDialogConfig();
      dialogConfig.data = agendamentoToEdit;
      dialogConfig.autoFocus = true;
      dialogConfig.id = 'editAgendamento';
      dialogConfig.width = '600px';
      dialogConfig.height = '400px';

    this.editaAgendamentoDialogRef = this.dialog.open(EditarAgendamentoComponent, dialogConfig);
  }

  naoEhPossivel() {
    this.alerta.open('Não é possível agir em um agendamento já Finalizado.', '',
      {
        duration: 5000,
        panelClass: 'info-snackbar'
      });
  }

  cancelarAgendamento(id) {
    const agendamento: Agendamento = this.agendamentos.find(a => a.id === id);

    if (agendamento.statusId === 3 || agendamento.statusId === 4) {
      this.naoEhPossivel();

      return 0;
    }

    const dialogConfig = new MatDialogConfig();
      dialogConfig.autoFocus = true;
      dialogConfig.id = 'cancelarAgendamento';
      dialogConfig.width = '450px';
      dialogConfig.height = '300px';

    this.cancelarAgendamentoDialogRef = this.dialog.open(CancelarAgendamentoComponent, dialogConfig);

    this.cancelarAgendamentoDialogRef.afterClosed()
      .subscribe(res => {
        let motivo;
        if (res) {
          if (res === 1) {
            motivo = 'outro';
          } else if (res === 2) {
            motivo = 'nao compareceu';
          }

          const cancelamento: CancelarAgendamento = {
            agendamentoid: agendamento.id,
            motivo: motivo
          };

          this.funcService.cancelarAgendamento(cancelamento)
            .subscribe(response => {
              this.alerta.open('Agendamento Cancelado com sucesso!', '', {
                panelClass: 'success-snackbar',
                duration: 5000
              });
            }, error => {
              console.log(error);
            }, () => {
              this.router.navigate(['/funcionarios/home']);
            });
        }
      });
  }

  finalizarAgendamento(id: number) {
    const agendamento = this.agendamentos.find(a => a.id === id);

    if (agendamento.statusId === 3 || agendamento.statusId === 4) {
      this.naoEhPossivel();

      return 0;
    }

    if (agendamento.tipoAgendamento === 1) {
      this.alerta.open('Consultas só são finalizadas na janela de Consultas.', '',
      {
        duration: 5000,
        panelClass: 'info-snackbar'
      });

      return 0;
    }

    const dialogConfig = new MatDialogConfig();
      dialogConfig.autoFocus = true;
      dialogConfig.data = agendamento;
      dialogConfig.id = 'finalizarAgendamento';
      dialogConfig.width = '450px';
      dialogConfig.height = '350px';

    this.finalizarAgendamentoDialogRef = this.dialog.open(FinalizarAgendamentoComponent, dialogConfig);

  }

}
