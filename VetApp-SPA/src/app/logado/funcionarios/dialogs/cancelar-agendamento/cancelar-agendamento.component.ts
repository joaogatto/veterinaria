import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-cancelar-agendamento',
  templateUrl: './cancelar-agendamento.component.html',
  styleUrls: ['./cancelar-agendamento.component.css']
})
export class CancelarAgendamentoComponent implements OnInit {
  motivo = 0;

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  cancelar() {
    const cancelarAgendamentoDialogRef: MatDialogRef<CancelarAgendamentoComponent> = this.dialog.getDialogById('cancelarAgendamento');

    cancelarAgendamentoDialogRef.close(this.motivo);
  }

}
