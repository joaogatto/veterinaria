import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { FuncionarioToAgendamento } from '../../../../_models/FuncionarioToAgendamento';
import { FuncionariosService } from '../../../../_services/funcionarios.service';
import { MatDialogRef, MatDialog, MatDialogConfig, MatSnackBar, MAT_DIALOG_DATA } from '@angular/material';
import { SelecionaClienteComponent } from '../seleciona-cliente/seleciona-cliente.component';
import { ClienteLista } from '../../../../_models/ClienteLista';
import { KeyValuePair } from '../../../../_models/KeyValuePair';
import { SelecionaAnimalComponent } from '../seleciona-animal/seleciona-animal.component';
import { SpinnerComponent } from '../../../spinner/spinner.component';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { Agendamento } from '../../../../_models/Agendamento';

@Component({
  selector: 'app-cadastrar-agendamento',
  templateUrl: './cadastrar-agendamento.component.html',
  styleUrls: ['./cadastrar-agendamento.component.css']
})
export class CadastrarAgendamentoComponent implements OnInit {
  isLinear = true;
  nomeCliente = 'Clique no botão ao lado para selecionar um Cliente';
  nomeAnimal = 'Clique no botão ao lado para selecionar um Animal';
  nomeFuncionario = 'Não Selecionado';
  agendamentosClinica: Agendamento[] = [];
  agendamentosFunc: Agendamento[] = [];
  hora = '';
  dataAgendamento: Date;

  agendamentoForm = this.fb.group({
    AnimalId: ['', Validators.required],
    FuncionarioId: ['', Validators.required],
    ClienteId: ['', Validators.required],
    TipoAgendamento: ['', Validators.required],
    DataHora: ['', Validators.required],
    Observacoes: ['']
  });

  funcionarios: FuncionarioToAgendamento[];
  funcionariosToShow: FuncionarioToAgendamento[];
  cliDialogRef: MatDialogRef<SelecionaClienteComponent>;
  animalDialogRef: MatDialogRef<SelecionaAnimalComponent>;
  spinnerDialogRef: MatDialogRef<SpinnerComponent>;
  clientes: ClienteLista[] = [];
  animais: KeyValuePair[];

  constructor(@Inject(MAT_DIALOG_DATA) public data: ClienteLista[],
    private fb: FormBuilder,
    private funcService: FuncionariosService,
    private dialog: MatDialog,
    private datePipe: DatePipe,
    private alerta: MatSnackBar,
    private router: Router) { }

  ngOnInit() {
    this.funcService.getFuncionariosToAgendamento()
      .subscribe(res => this.funcionarios = res);
    this.clientes = this.data;
    if (this.clientes.length === 1) {
      const cli: ClienteLista = this.clientes.find(c => c.id !== null);
      this.agendamentoForm.patchValue({
        ClienteId: cli.id
      });
      this.nomeCliente = cli.nome;
    }
  }

  selecionarCliente() {

    const dialogConfig = new MatDialogConfig();
      dialogConfig.data = this.clientes;
      dialogConfig.autoFocus = true;
      dialogConfig.id = 'selecionaCliente';
      dialogConfig.width = '600px';

    this.cliDialogRef = this.dialog.open(SelecionaClienteComponent, dialogConfig);

    this.cliDialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.nomeCliente = res.nome;
        this.agendamentoForm.patchValue({
          ClienteId: res.id
        });
      }
    });

  }

  selecionarAnimal() {
    const cli: ClienteLista = this.clientes.find(c => c.id === this.agendamentoForm.get('ClienteId').value);

    const dialogConfig = new MatDialogConfig();
      dialogConfig.data = cli;
      dialogConfig.autoFocus = true;
      dialogConfig.id = 'selecionaAnimal';
      dialogConfig.width = '600px';

      this.animalDialogRef = this.dialog.open(SelecionaAnimalComponent, dialogConfig);

      this.animalDialogRef.afterClosed().subscribe(res => {
        if (res) {
          this.nomeAnimal = res.nome;
          this.agendamentoForm.patchValue({
            AnimalId: res.id
          });
        }
      });

  }

  tipoAgendamentoChanged(tipoId) {
    this.funcionariosToShow = [];
    if (tipoId === 1) {
      this.funcionarios.forEach(func => {
        if (func.role === 'Veterinario') {
          this.funcionariosToShow.push(func);
        }
      });
    } else {
      this.funcionarios.forEach(func => {
        if (func.role !== 'Veterinario') {
          this.funcionariosToShow.push(func);
        }
      });
    }
  }

  setouData(dataFormatada: Date) {
    this.spinnerDialogRef = this.dialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: false
    });
    if (dataFormatada) {
      this.agendamentoForm.patchValue({
        DataHora: dataFormatada.getTime()
      });
      this.funcService.getAgendamentosPorData(dataFormatada.getTime())
        .subscribe(res => {
          this.agendamentosClinica = res;
        }, error => {
          this.alerta.open(error.error, '', {
            panelClass: 'error-snackbar',
            duration: 5000
          });
          this.spinnerDialogRef.close();
        }, () => this.spinnerDialogRef.close());
    } else {
      this.spinnerDialogRef.close();
    }
  }

  selecionouFuncionario(funcId) {
    this.agendamentosFunc = [];
    this.nomeFuncionario = this.funcionarios.find(f => f.id === funcId).nome;
    this.agendamentosClinica.forEach(agendamento => {
      if (agendamento.funcionario.id === funcId) {
        this.agendamentosFunc.push(agendamento);
      }
    });
  }

  agendar(hora) {
    this.spinnerDialogRef = this.dialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: false
    });

    const horario = hora.split(':');

    const dataHoraMilliSeconds = this.dataAgendamento.setHours(horario[0] - 3, horario[1]);

    this.agendamentoForm.patchValue({
      DataHora: dataHoraMilliSeconds
    });


    this.funcService.cadastrarAgendamento(this.agendamentoForm.value)
      .subscribe(res => {
        this.alerta.open('Agendamento cadastrado com sucesso!', '', {
          panelClass: 'success-snackbar',
          duration: 5000
        });
      }, error => {
        this.alerta.open('Erro ao cadastrar agendamento', '', {
          panelClass: 'error-snackbar',
          duration: 5000
        });
        this.spinnerDialogRef.close();
      }, () => {
        this.router.navigate(['/funcionarios/home']);
        this.dialog.closeAll();
      });
  }
}
