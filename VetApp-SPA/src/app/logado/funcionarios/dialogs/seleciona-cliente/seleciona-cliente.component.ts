import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { ClienteLista } from '../../../../_models/ClienteLista';
import { MAT_DIALOG_DATA, MatSelectionList, MatListOption, MatDialogRef, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { KeyValuePair } from '../../../../_models/KeyValuePair';

@Component({
  selector: 'app-seleciona-cliente',
  templateUrl: './seleciona-cliente.component.html',
  styleUrls: ['./seleciona-cliente.component.css']
})
export class SelecionaClienteComponent implements OnInit {
  termo = '';
  cliSelecionado: number;
  clientes: ClienteLista[] = [];
  @ViewChild(MatSelectionList) selectionList: MatSelectionList;
  clienteReturn: KeyValuePair;
  selecionaClienteDialogRef: MatDialogRef<SelecionaClienteComponent>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: ClienteLista[],
    private dialog: MatDialog) { }

  ngOnInit() {
    this.selectionList.selectedOptions = new SelectionModel<MatListOption>(false);
    this.clientes = this.data;
  }

  termoChanged(termo) {
    this.termo = termo;
  }

  clienteMudou(clienteId) {
    this.cliSelecionado = clienteId;
    const cliente: ClienteLista = this.clientes.find(c => c.id === this.cliSelecionado);

    this.clienteReturn = {
      id: cliente.id,
      nome: cliente.nome
    };

    this.selecionaClienteDialogRef = this.dialog.getDialogById('selecionaCliente');

    this.selecionaClienteDialogRef.close(this.clienteReturn);
  }

}
