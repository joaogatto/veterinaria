import { Component, OnInit, Inject } from '@angular/core';
import { Agendamento } from '../../../../_models/Agendamento';
import { MAT_DIALOG_DATA, MatSnackBar, MatDialogRef, MatDialog } from '@angular/material';
import { FuncionariosService } from '../../../../_services/funcionarios.service';
import { Router } from '@angular/router';
import { SpinnerComponent } from '../../../spinner/spinner.component';

@Component({
  selector: 'app-finalizar-agendamento',
  templateUrl: './finalizar-agendamento.component.html',
  styleUrls: ['./finalizar-agendamento.component.css']
})
export class FinalizarAgendamentoComponent implements OnInit {
  agendamento: Agendamento;
  foto: File;
  spinnerDialogRef: MatDialogRef<SpinnerComponent>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: Agendamento,
    public alerta: MatSnackBar,
    public funcService: FuncionariosService,
    public router: Router,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.agendamento = this.data;
  }

  inserirImagem(event) {
    if (event !== undefined) {
      if (event.target.files.length > 1) {
        this.alerta.open('Não é possível inserir mais de uma imagem.', '', {
            duration: 3000,
            panelClass: 'error-snackbar'
        });

        return 0;
      }

      this.foto = event.target.files[0];

      if (this.foto.type !== 'image/jpeg') {
        this.alerta.open('Formato de arquivo incorreto. Só aceitamos JPEG ou JPG', '', {
          duration: 3000,
          panelClass: 'error-snackbar'
      });

      this.foto = undefined;
      return 0;
      }

      if (this.foto.size / 1000 > 1000) {
        this.alerta.open('Tamanho máximo excedido. Tamanho máximo = 1mb', '', {
          duration: 3000,
          panelClass: 'error-snackbar'
      });

      this.foto = undefined;
      return 0;
      }
    }

    return 0;
  }

  finalizarAgendamento() {
    this.spinnerDialogRef = this.dialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: true
    });

    this.funcService.finalizarAgendamento(this.agendamento.id, this.foto)
      .subscribe(res => {
        this.alerta.open('Agendamento Finalizado com sucesso!', '', {
          panelClass: 'success-snackbar',
          duration: 5000
        });
        this.spinnerDialogRef.close();
      }, error => {
        console.log(error);
      }, () => {
        this.dialog.closeAll();
        this.router.navigate(['funcionarios/home']);
      });
  }

}
