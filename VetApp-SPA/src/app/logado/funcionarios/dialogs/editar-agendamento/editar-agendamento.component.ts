import { Component, OnInit, Inject } from '@angular/core';
import { Agendamento } from '../../../../_models/Agendamento';
import { Validators, FormBuilder } from '@angular/forms';
import { FuncionarioToAgendamento } from '../../../../_models/FuncionarioToAgendamento';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatSnackBar } from '@angular/material';
import { SelecionaClienteComponent } from '../seleciona-cliente/seleciona-cliente.component';
import { SelecionaAnimalComponent } from '../seleciona-animal/seleciona-animal.component';
import { SpinnerComponent } from '../../../spinner/spinner.component';
import { ClienteLista } from '../../../../_models/ClienteLista';
import { KeyValuePair } from '../../../../_models/KeyValuePair';
import { FuncionariosService } from '../../../../_services/funcionarios.service';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editar-agendamento',
  templateUrl: './editar-agendamento.component.html',
  styleUrls: ['./editar-agendamento.component.css']
})
export class EditarAgendamentoComponent implements OnInit {
  isLinear = false;
  nomeCliente = '';
  nomeAnimal = '';
  nomeFuncionario = 'fdasafdsasfd';
  agendamentosClinica: Agendamento[] = [];
  agendamentosFunc: Agendamento[] = [];
  hora = '';
  dataAgendamento: Date;

  agendamentoForm = this.fb.group({
    AnimalId: ['', Validators.required],
    FuncionarioId: ['', Validators.required],
    ClienteId: ['', Validators.required],
    TipoAgendamento: ['', Validators.required],
    DataHora: ['', Validators.required],
    Observacoes: ['']
  });

  funcionarios: FuncionarioToAgendamento[];
  funcionariosToShow: FuncionarioToAgendamento[];
  cliDialogRef: MatDialogRef<SelecionaClienteComponent>;
  animalDialogRef: MatDialogRef<SelecionaAnimalComponent>;
  spinnerDialogRef: MatDialogRef<SpinnerComponent>;
  clientes: ClienteLista[];
  animais: KeyValuePair[];

  constructor(@Inject(MAT_DIALOG_DATA) public data: Agendamento,
    private fb: FormBuilder,
    private funcService: FuncionariosService,
    private dialog: MatDialog,
    private datePipe: DatePipe,
    private alerta: MatSnackBar,
    private router: Router) { }

  ngOnInit() {
    this.funcService.getFuncionariosToAgendamento()
      .subscribe(res => this.funcionarios = res);
    this.agendamentoForm.patchValue({
      AnimalId: this.data.animal.id,
      FuncionarioId: this.data.funcionario.id,
      ClienteId: this.data.cliente.id,
      TipoAgendamento: this.data.tipoAgendamento,
      DataHora: new Date(this.data.dataHora).toDateString(),
      Observacoes: this.data.observacoes
    });
    this.nomeAnimal = this.data.animal.nome;
    this.nomeCliente = this.data.cliente.nome;
    this.nomeFuncionario = this.data.funcionario.nome;
    this.dataAgendamento = new Date(this.data.dataHora);
    this.hora = new Date(this.data.dataHora).toLocaleTimeString();

    setTimeout(() => {
      this.carregaGrid();
    }, 1500);
  }

  carregaGrid() {
    this.tipoAgendamentoChanged(this.data.tipoAgendamento);
    this.setouData(this.dataAgendamento);
    this.selecionouFuncionario(this.data.funcionario.id);
  }

  tipoAgendamentoChanged(tipoId) {
    this.funcionariosToShow = [];
    if (tipoId === 1) {
      this.funcionarios.forEach(func => {
        if (func.role === 'Veterinario') {
          this.funcionariosToShow.push(func);
        }
      });
    } else {
      this.funcionarios.forEach(func => {
        if (func.role !== 'Veterinario') {
          this.funcionariosToShow.push(func);
        }
      });
    }
  }

  setouData(dataFormatada: Date) {
    this.spinnerDialogRef = this.dialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: false
    });
    if (dataFormatada) {
      this.agendamentoForm.patchValue({
        DataHora: dataFormatada.getTime()
      });
      this.funcService.getAgendamentosPorData(dataFormatada.getTime())
        .subscribe(res => {
          this.agendamentosClinica = res;
        }, error => {
          this.alerta.open(error.error, '', {
            panelClass: 'error-snackbar',
            duration: 5000
          });
          this.spinnerDialogRef.close();
        }, () => this.spinnerDialogRef.close());
    } else {
      this.spinnerDialogRef.close();
    }
  }

  selecionouFuncionario(funcId) {
    this.agendamentosFunc = [];
    this.nomeFuncionario = this.funcionarios.find(f => f.id === funcId).nome;
    this.agendamentosClinica.forEach(agendamento => {
      if (agendamento.funcionario.id === funcId) {
        this.agendamentosFunc.push(agendamento);
      }
    });
  }

  agendar(hora) {
    this.spinnerDialogRef = this.dialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: true
    });

    const horario = hora.split(':');

    const dataHoraMilliSeconds = this.dataAgendamento.setHours(horario[0] - 3, horario[1]);

    this.agendamentoForm.patchValue({
      DataHora: dataHoraMilliSeconds
    });


    this.funcService.atualizarAgendamento(this.data.id, this.agendamentoForm.value)
      .subscribe(res => {
        this.alerta.open('Agendamento editado com sucesso!', '', {
          panelClass: 'success-snackbar',
          duration: 5000
        });
      }, error => {
        console.log(error);
        this.alerta.open('Erro ao editar agendamento', '', {
          panelClass: 'error-snackbar',
          duration: 5000
        });
        this.spinnerDialogRef.close();
      }, () => {
        this.router.navigate(['/funcionarios/home']);
        this.dialog.closeAll();
      });
  }
}
