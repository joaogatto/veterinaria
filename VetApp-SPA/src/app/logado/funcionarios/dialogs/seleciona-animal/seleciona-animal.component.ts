import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatListOption, MatSelectionList, MatDialogRef, MatDialog } from '@angular/material';
import { ClienteLista } from '../../../../_models/ClienteLista';
import { SelectionModel } from '@angular/cdk/collections';
import { KeyValuePair } from '../../../../_models/KeyValuePair';

@Component({
  selector: 'app-seleciona-animal',
  templateUrl: './seleciona-animal.component.html',
  styleUrls: ['./seleciona-animal.component.css']
})
export class SelecionaAnimalComponent implements OnInit {

  @ViewChild(MatSelectionList) selectionList: MatSelectionList;
  cliente: ClienteLista;
  termo = '';
  animalSelecionado: number;
  selecionaAnimalDialogRef: MatDialogRef<SelecionaAnimalComponent>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: ClienteLista,
    private dialog: MatDialog) { }

  ngOnInit() {
    console.log(this.data);
    this.selectionList.selectedOptions = new SelectionModel<MatListOption>(false);
    this.cliente = this.data;
  }

  termoChanged(termo) {
    this.termo = termo;
  }

  animalMudou(animalId) {
    this.animalSelecionado = animalId;

    const animal: KeyValuePair = this.cliente.animais.find(a => a.id === this.animalSelecionado);

    this.selecionaAnimalDialogRef = this.dialog.getDialogById('selecionaAnimal');

    this.selecionaAnimalDialogRef.close(animal);
  }

}
