import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatSelectionList, MatListOption, MatDialogRef, MatDialog, MatDialogConfig, MatSnackBar } from '@angular/material';
import { ClienteLista } from '../../../../_models/ClienteLista';
import { FormBuilder, Validators } from '@angular/forms';
import { SelectionModel } from '@angular/cdk/collections';
import { FuncionariosService } from '../../../../_services/funcionarios.service';
import { Especie } from '../../../../_models/Especie';
import { KeyValuePair } from '../../../../_models/KeyValuePair';
import { SelecionaClienteComponent } from '../../dialogs/seleciona-cliente/seleciona-cliente.component';
import { SpinnerComponent } from '../../../spinner/spinner.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastrar-animal',
  templateUrl: './cadastrar-animal.component.html',
  styleUrls: ['./cadastrar-animal.component.css']
})
export class CadastrarAnimalComponent implements OnInit {
  cadastrarAnimalForm = this.fb.group({
    Nome: ['', Validators.required],
    DataNascimento: ['', Validators.required],
    Cor: ['', Validators.required],
    ClienteId: ['', Validators.required],
    RacaId: ['', Validators.required],
  });
  clientes: ClienteLista[];
  clienteSelecionado: ClienteLista;
  cliSelecionado: number;
  termo = '';
  nomeCliente = 'É necessário inserir um Cliente';
  especies: Especie[] = [];
  especie: Especie;
  racas: KeyValuePair[] = [];
  racaId: number;
  @ViewChild(MatSelectionList) selectionList: MatSelectionList;
  cliDialogRef: MatDialogRef<SelecionaClienteComponent>;
  spinnerDialogRef: MatDialogRef<SpinnerComponent>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: ClienteLista[],
    private fb: FormBuilder,
    private funcService: FuncionariosService,
    private dialog: MatDialog,
    private router: Router,
    private alerta: MatSnackBar) { }

  ngOnInit() {
    this.selectionList.selectedOptions = new SelectionModel<MatListOption>(false);
    this.clientes = this.data;
    if (this.clientes.length <= 1) {
      this.cadastrarAnimalForm.patchValue({
        ClienteId: this.clientes[0].id
      });
      this.nomeCliente = this.clientes[0].nome;
    } else {
      console.log('veio da home');
    }

    this.funcService.getEspecies().subscribe(res => {
      this.especies = res;
    }, error => {
      console.log(error);
    }, () => {
      this.carregaRacas(0);
    });
  }

  termoChanged(termo) {
    this.termo = termo;
  }

  carregaRacas(especieId: number) {
    this.racas = [];
    this.cadastrarAnimalForm.patchValue({
      RacaId: null
    });
    this.racaId = null;

    if (especieId === 0) {
      this.especies.forEach(esp => {
        esp.racas.forEach(raca => {
          this.racas.push(raca);
        });
      });
    } else {
      this.especie = this.especies.find(esp => esp.id === especieId);
      if (this.especie.racas) {
        this.especie.racas.forEach(raca => {
          this.racas.push(raca);
        });
      }
    }
  }

  racaChanged(racaId) {
    this.racaId = racaId;
    this.cadastrarAnimalForm.patchValue({
      RacaId: this.racaId
    });
  }

  selecionarCliente() {
    const dialogConfig = new MatDialogConfig();
      dialogConfig.data = this.clientes;
      dialogConfig.autoFocus = true;
      dialogConfig.id = 'selecionaCliente';
      dialogConfig.width = '600px';

    this.cliDialogRef = this.dialog.open(SelecionaClienteComponent, dialogConfig);

    this.cliDialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.nomeCliente = res.nome;
        this.cadastrarAnimalForm.patchValue({
          ClienteId: res.id
        });
      }
    });
  }

  cadastrarAnimal() {
    this.spinnerDialogRef = this.dialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: true
    });

    this.funcService.cadastrarAnimal(this.cadastrarAnimalForm.value)
      .subscribe(res => {
        this.alerta.open('Animal cadastrado com sucesso!', '', {
          duration: 5000,
          panelClass: 'success-snackbar'
        });
      }, error => {
        this.alerta.open(error.error, '', {
          duration: 5000,
          panelClass: 'error-snackbar'
        });
        this.dialog.closeAll();
      }, () => {
        this.dialog.closeAll();
        this.router.navigate(['/funcionarios/home']);
      });

  }

}
