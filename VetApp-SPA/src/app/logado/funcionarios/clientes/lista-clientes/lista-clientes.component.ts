import { Component, OnInit } from '@angular/core';
import { ClienteLista } from '../../../../_models/ClienteLista';
import { FuncionariosService } from '../../../../_services/funcionarios.service';
import { MatSnackBar, MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import { SpinnerComponent } from '../../../spinner/spinner.component';
import { PerfilClienteComponent } from '../perfil-cliente/perfil-cliente.component';
import { CadastrarAnimalComponent } from '../../animais/cadastrar-animal/cadastrar-animal.component';
import { CadastrarAgendamentoComponent } from '../../dialogs/cadastrar-agendamento/cadastrar-agendamento.component';

@Component({
  selector: 'app-lista-clientes',
  templateUrl: './lista-clientes.component.html',
  styleUrls: ['./lista-clientes.component.css']
})
export class ListaClientesComponent implements OnInit {
  clientes: ClienteLista[] = [];
  termo: any = '';
  clientePerfil: ClienteLista;
  perfilDialogRef: MatDialogRef<PerfilClienteComponent>;
  cadAnimalDialogRef: MatDialogRef<CadastrarAnimalComponent>;
  spinnerDialogRef: MatDialogRef<SpinnerComponent>;
  cadAgendamentoDialogRef: MatDialogRef<CadastrarAgendamentoComponent>;
  clienteToAddAnimal: ClienteLista[];

  constructor(private funcService: FuncionariosService,
    private alerta: MatSnackBar,
    private dialog: MatDialog,
    private perfilDialog: MatDialog) { }

  ngOnInit() {

    this.funcService.getClientes().subscribe(res => {
      this.clientes = res;
    }, error => {
      this.alerta.open(error, '', {
        duration: 5000,
        panelClass: 'error-snackbar'
      });
      this.dialog.closeAll();
    }, () => {
      this.dialog.closeAll();
    });

  }

  termoChanged(termo) {
    this.termo = termo;
  }

  verPerfil(id) {
    const dialogConfig = new MatDialogConfig();
    this.clientePerfil = this.clientes.find(c => c.id === id);

    dialogConfig.data = this.clientePerfil;
    dialogConfig.autoFocus = true;
    dialogConfig.id = 'perfil';
    dialogConfig.width = '600px';
    dialogConfig.height = '400px';

    this.perfilDialogRef = this.perfilDialog.open(PerfilClienteComponent, dialogConfig);
  }

  cadastrarAnimal(id) {
    const clienteToFind: ClienteLista = this.clientes.find(c => c.id === id);
    this.clienteToAddAnimal = [];
    this.clienteToAddAnimal.push(clienteToFind);

    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = this.clienteToAddAnimal;
    dialogConfig.autoFocus = true;
    dialogConfig.id = 'cadAnimal';
    dialogConfig.width = '600px';
    dialogConfig.height = '400px';

    this.cadAnimalDialogRef = this.perfilDialog.open(CadastrarAnimalComponent, dialogConfig);
  }

  cadastrarAgendamento(id) {
    this.spinnerDialogRef = this.dialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: true
    });

    const clientes: ClienteLista[] = [];
    clientes.push(this.clientes.find(c => c.id === id));
    this.dialog.closeAll();

    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = clientes;
    dialogConfig.autoFocus = true;
    dialogConfig.id = 'cadAgendamento';
    dialogConfig.width = '600px';
    dialogConfig.height = '400px';

    this.cadAgendamentoDialogRef = this.dialog.open(CadastrarAgendamentoComponent, dialogConfig);

  }

}
