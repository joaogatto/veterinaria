import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar, MatDialog, MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { SpinnerComponent } from '../../../spinner/spinner.component';
import { FuncionariosService } from '../../../../_services/funcionarios.service';

@Component({
  selector: 'app-vincular-cliente',
  templateUrl: './vincular-cliente.component.html',
  styleUrls: ['./vincular-cliente.component.css']
})
export class VincularClienteComponent implements OnInit {
  vinculoCliCliForm = this.fb.group({
    Email: ['', Validators.required],
    Senha: ['', Validators.required]
  });
  constructor(private fb: FormBuilder,
    private alerta: MatSnackBar,
    private router: Router,
    private spinDialog: MatDialog,
    private funcService: FuncionariosService) { }

  ngOnInit() {
  }

  vincularCliCli() {
    const spinDialogRef: MatDialogRef<SpinnerComponent> = this.spinDialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: true
    });

    this.funcService.vincularCliente(this.vinculoCliCliForm.value)
      .subscribe(res => {
        this.alerta.open('Cliente vinculado com sucesso!', '', {
          duration: 5000,
          panelClass: 'success-snackbar'
        });
        this.router.navigate(['/funcionarios/home']);
      }, error => {
        this.alerta.open(error.error, '', {
          duration: 5000,
          panelClass: 'error-snackbar'
        });
        this.spinDialog.closeAll();
      }, () => this.spinDialog.closeAll());

  }

}
