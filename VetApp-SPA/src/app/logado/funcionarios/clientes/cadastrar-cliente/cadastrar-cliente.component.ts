import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar, MatDialog, MatDialogRef } from '@angular/material';
import { FuncionariosService } from '../../../../_services/funcionarios.service';
import { Router } from '@angular/router';
import { SpinnerComponent } from '../../../spinner/spinner.component';

@Component({
  selector: 'app-cadastrar-cliente',
  templateUrl: './cadastrar-cliente.component.html',
  styleUrls: ['./cadastrar-cliente.component.css']
})
export class CadastrarClienteComponent implements OnInit {
  novoClienteForm = this.fb.group({
    Nome: ['', Validators.required],
    DataNascimento: ['', Validators.required],
    Endereco: ['', Validators.required],
    Bairro: ['', Validators.required],
    Telefone: ['', Validators.required],
    Celular: [''],
    OutroNumero: [''],
    Email: ['', [Validators.required, Validators.email]],
    Senha: ['', [Validators.required]]
  });
  confirmarSenha = '';

  constructor(private fb: FormBuilder,
    private alerta: MatSnackBar,
    private funcService: FuncionariosService,
    private router: Router,
    private spinDialog: MatDialog) { }

  ngOnInit() {
  }

  cadastrarCliente() {
    const spinDialogRef: MatDialogRef<SpinnerComponent> = this.spinDialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: true
    });
    const cli = this.novoClienteForm.value;
    if (cli.Senha === this.confirmarSenha) {
      this.funcService.cadastrarCliente(this.novoClienteForm.value)
        .subscribe(res => {
          this.alerta.open('Cliente cadastrado com sucesso!', '', {
            duration: 5000,
            panelClass: 'success-snackbar'
          });
          this.router.navigate(['/funcionarios/home']);
        }, error => {
          this.alerta.open(error.error, '', {
            duration: 5000,
            panelClass: 'error-snackbar'
          });
          this.spinDialog.closeAll();
        }, () => this.spinDialog.closeAll());
    } else {
      this.spinDialog.closeAll();
      this.alerta.open('Senha e Confirmar Senha devem ser Iguais', '', {
        duration: 5000,
        panelClass: 'error-snackbar'
      });
    }
  }

  inputConfirmarSenha(campo) {
    this.confirmarSenha = campo;
  }

}
