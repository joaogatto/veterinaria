import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import { ClienteLista } from '../../../../_models/ClienteLista';
import { EditarClienteComponent } from '../editar-cliente/editar-cliente.component';

@Component({
  selector: 'app-perfil-cliente',
  templateUrl: './perfil-cliente.component.html',
  styleUrls: ['./perfil-cliente.component.css']
})
export class PerfilClienteComponent implements OnInit {
  cliente: ClienteLista;
  editDialogRef: MatDialogRef<EditarClienteComponent>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: ClienteLista,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.cliente = this.data;
  }

  editarCliente() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = this.cliente;
    dialogConfig.autoFocus = true;
    dialogConfig.id = 'editar';

    this.editDialogRef = this.dialog.open(EditarClienteComponent, dialogConfig);
  }
}
