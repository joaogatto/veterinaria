import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatSnackBar, MatDialogRef } from '@angular/material';
import { ClienteLista } from '../../../../_models/ClienteLista';
import { FormBuilder, Validators } from '@angular/forms';
import { FuncionariosService } from '../../../../_services/funcionarios.service';
import { Router } from '@angular/router';
import { SpinnerComponent } from '../../../spinner/spinner.component';

@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html',
  styleUrls: ['./editar-cliente.component.css']
})
export class EditarClienteComponent implements OnInit {
  clienteEdit: ClienteLista;
  editaClienteForm = this.fb.group({
    Id: ['', Validators.required],
    Nome: ['', Validators.required],
    DataNascimento: ['', Validators.required],
    Endereco: ['', Validators.required],
    Bairro: ['', Validators.required],
    Telefone: ['', Validators.required],
    Celular: [''],
    OutroNumero: ['']
  });
  dialogRef: MatDialogRef<EditarClienteComponent>;
  spinDialogRef: MatDialogRef<SpinnerComponent>;

  constructor(@Inject(MAT_DIALOG_DATA) public data: ClienteLista,
  private dialog: MatDialog,
  private fb: FormBuilder,
  private funcService: FuncionariosService,
  private alerta: MatSnackBar,
  private router: Router) { }

  ngOnInit() {
    this.clienteEdit = this.data;
    this.editaClienteForm.setValue({
      Id: this.clienteEdit.id,
      Nome: this.clienteEdit.nome,
      DataNascimento: this.clienteEdit.dataNascimento,
      Endereco: this.clienteEdit.endereco,
      Bairro: this.clienteEdit.bairro,
      Telefone: this.clienteEdit.telefone,
      Celular: this.clienteEdit.celular,
      OutroNumero: this.clienteEdit.outroNumero
    });

  }

  editaCliente() {
    this.spinDialogRef = this.dialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: true
    });
    this.funcService.editarCliente(this.editaClienteForm.value)
        .subscribe(res => {
          this.alerta.open('Cliente editado com sucesso!', '', {
            duration: 5000,
            panelClass: 'success-snackbar'
          });
          this.router.navigate(['/funcionarios/home']);
        }, error => {
          this.alerta.open(error.error, '', {
            duration: 5000,
            panelClass: 'error-snackbar'
          });
          this.spinDialogRef.close();
        }, () => this.dialog.closeAll());
  }

}
