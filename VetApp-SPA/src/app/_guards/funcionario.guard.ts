import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../_services/login.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class FuncionarioGuard implements CanActivate {
  jwtHelper = new JwtHelperService();
  token: any;

  constructor(private loginService: LoginService, private router: Router) {}

  canActivate(): boolean {
    if (this.loginService.logado()) {
      this.token = this.loginService.tokenDecript;
      if (this.token.role === 'Gerente' || this.token.role === 'Funcionario' || this.token.role === 'Veterinario') {
        return true;
      }
    }

    console.log('adminguard');
    console.log('Você não possui acesso a essa página.', 'Erro');
    return false;
  }
}
