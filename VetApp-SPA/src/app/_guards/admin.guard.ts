import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LoginService } from '../_services/login.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  jwtHelper = new JwtHelperService();
  token: any;

  constructor(private loginService: LoginService, private router: Router) {}

  canActivate(): boolean {
    if (this.loginService.logado()) {
      this.token = this.loginService.tokenDecript;
      if (this.token.role === 'Admin') {
        return true;
      }
    }

    console.log('adminguard');
    console.log('Você não possui acesso a essa página.', 'Erro');
    return false;
  }
}
