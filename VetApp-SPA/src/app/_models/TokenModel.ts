export class TokenModel {
    nameid: string;
    clinicaId: string;
    unique_name: string;
    role: string;
    nbf: number;
    exp: number;
    iat: number;
}
