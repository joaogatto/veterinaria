import { KeyValuePair } from './KeyValuePair';

export class VacinaReturn {
    id: number;
    nomeVacina: string;
    quantidadeAplicada: number;
    dataHoraAplicacao: Date;
    animal: KeyValuePair;
}
