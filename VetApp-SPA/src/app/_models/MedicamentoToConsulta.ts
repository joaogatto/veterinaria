export interface MedicamentoToConsulta {
    nome: string;
    quantidade: number;
    unidadeDeMedida: string;
    comoSerTomado: string;
}
