import { KeyValuePair } from './KeyValuePair';

export class Especie {
    id: number;
    nome: string;
    racas: KeyValuePair[];
}
