import { KeyValuePair } from './KeyValuePair';

export interface Agendamento {
    id: number;
    animal: KeyValuePair;
    cliente: KeyValuePair;
    tipoAgendamento: number;
    dataHora: Date;
    funcionario: KeyValuePair;
    observacoes: string;
    statusId: number;
}
