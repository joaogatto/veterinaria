export interface ExameToConsulta {
    id: number;
    nome: string;
    ehPedido: boolean;
}
