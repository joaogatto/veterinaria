import { KeyValuePair } from './KeyValuePair';

export class AgendamentoToConsulta {
    // public int Id { get; set; }
    //     public KeyValuePair Cliente { get; set; }
    //     public KeyValuePair Animal { get; set; }
    //     public KeyValuePair RacaAnimal { get; set; }
    //     public DateTime DataNascAnimal { get; set; }

    //     public string Observacoes { get; set; }

    id: number;
    cliente: KeyValuePair;
    animal: KeyValuePair;
    racaAnimal: KeyValuePair;
    dataNascAnimal: Date;
    observacoes: string;
}
