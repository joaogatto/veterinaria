import { KeyValuePair } from './KeyValuePair';

export interface VacinaToVinculo {
    id: number;
    vacina: KeyValuePair;
    animal: KeyValuePair;
    quantidadeAplicada: number;
    dataHoraAplicacao: Date;
}
