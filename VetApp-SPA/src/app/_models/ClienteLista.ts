import { KeyValuePair } from './KeyValuePair';

export interface ClienteLista {
    id: number;
    nome: string;
    dataNascimento: Date;
    endereco: string;
    bairro: string;
    telefone: string;
    celular: string;
    outroNumero: string;
    email: string;
    animais: KeyValuePair[];
}
