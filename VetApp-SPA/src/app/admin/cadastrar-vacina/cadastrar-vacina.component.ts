import { Component, OnInit } from '@angular/core';
import { AdminService } from '../../_services/admin.service';
import { Router } from '@angular/router';
import { KeyValuePair } from '../../_models/KeyValuePair';

@Component({
  selector: 'app-cadastrar-vacina',
  templateUrl: './cadastrar-vacina.component.html',
  styleUrls: ['./cadastrar-vacina.component.css']
})
export class CadastrarVacinaComponent implements OnInit {
  nomeVacina = '';
  vacina: KeyValuePair = {
    id: 0,
    nome: ''
  };

  constructor(private admService: AdminService,
    private router: Router) { }

  ngOnInit() {
  }

  cadastrarVacina() {
    this.vacina.nome = this.nomeVacina;
    this.admService.cadastrarVacina(this.vacina)
      .subscribe(res => this.router.navigate(['/admin/home']), error => console.log(error));
  }
}
