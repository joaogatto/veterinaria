import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AdminService } from '../../_services/admin.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastrar-clinica',
  templateUrl: './cadastrar-clinica.component.html',
  styleUrls: ['./cadastrar-clinica.component.css']
})
export class CadastrarClinicaComponent implements OnInit {
  clinicaForm = this.fb.group({
    ClinicaNome: ['', Validators.required],
    Responsavel: ['', Validators.required],
    Endereco: ['', Validators.required],
    ClinicaLogin: ['', Validators.required],
    Senha: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20)]],
    Funcionario: this.fb.group({
      Nome: ['', Validators.required],
      Email: ['', [Validators.required, Validators.email]],
      Senha: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(20)]],
      Role: ['Gerente']
    })
  });

  constructor(private fb: FormBuilder,
    private adminService: AdminService,
    private alerta: MatSnackBar,
    private router: Router) { }

  ngOnInit() {
  }

  cadastrarClinica() {
    this.adminService.cadastrarClinica(this.clinicaForm.value)
      .subscribe(res => {
        console.log(res);
        this.router.navigate(['/admin/home']);
        this.alerta.open('Clínica cadastrada com sucesso!', '', {
          duration: 5000,
          panelClass: 'success-snackbar'
        });
      }, error => {
        console.log(error);
        this.alerta.open('Não foi possível cadastrar a clínica', '', {
          duration: 5000,
          panelClass: 'error-snackbar'
        });
      });
  }

}
