import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AdminGuard } from './_guards/admin.guard';
import { HomeAdminComponent } from './admin/home-admin/home-admin.component';
import { CadastrarClinicaComponent } from './admin/cadastrar-clinica/cadastrar-clinica.component';
import { ListaClientesComponent } from './logado/funcionarios/clientes/lista-clientes/lista-clientes.component';
import { FuncionarioGuard } from './_guards/funcionario.guard';
import { HomeFuncionariosComponent } from './logado/funcionarios/home-funcionarios/home-funcionarios.component';
import { TabsClientesComponent } from './logado/funcionarios/clientes/tabs-clientes/tabs-clientes.component';
import { GerenteGuard } from './_guards/gerente.guard';
import { CadastrarFuncionarioComponent } from './logado/gerentes/cadastrar-funcionario/cadastrar-funcionario.component';
import { ListaAgendamentosComponent } from './logado/funcionarios/agendamentos/lista-agendamentos/lista-agendamentos.component';
import { TabsAgendamentosComponent } from './logado/funcionarios/agendamentos/tabs-agendamentos/tabs-agendamentos.component';
import { TabsConsultasComponent } from './logado/veterinarios/tabs-consultas/tabs-consultas.component';
import { VeterinarioGuard } from './_guards/veterinario.guard';
import { ConsultaComponent } from './logado/veterinarios/consulta/consulta.component';
import { CadastrarVacinaComponent } from './admin/cadastrar-vacina/cadastrar-vacina.component';

export const appRoute: Routes = [
    { path: '', component: AppComponent },
    {
        path: 'admin',
        runGuardsAndResolvers: 'always',
        canActivate: [AdminGuard],
        children: [
            { path: 'home', component: HomeAdminComponent},
            { path: 'cadastrarclinica', component: CadastrarClinicaComponent},
            { path: 'cadastrarvacina', component: CadastrarVacinaComponent},
        ]
    },
    {
        path: 'funcionarios',
        runGuardsAndResolvers: 'always',
        canActivate: [FuncionarioGuard],
        children: [
            { path: 'home', component: HomeFuncionariosComponent},
            { path: 'clientes', component: TabsClientesComponent},
            { path: 'agendamentos', component: TabsAgendamentosComponent}
        ]
    },
    {
        path: 'veterinarios',
        runGuardsAndResolvers: 'always',
        canActivate: [VeterinarioGuard],
        children: [
            { path: 'consultas', component: TabsConsultasComponent},
            { path: 'gerarConsulta', component: ConsultaComponent},
            { path: 'gerarConsulta/:agendamentoId', component: ConsultaComponent},
        ]
    },
    {
        path: 'gerente',
        runGuardsAndResolvers: 'always',
        canActivate: [GerenteGuard],
        children: [
            { path: 'cadastrarfuncionario', component: CadastrarFuncionarioComponent},
        ]
    },
    { path: 'home', component: AppComponent},
    { path: '**', redirectTo: '/home', pathMatch: 'full'}
];
