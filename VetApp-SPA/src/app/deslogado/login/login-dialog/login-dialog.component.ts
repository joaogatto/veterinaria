import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../../../_services/login.service';
import { MatDialogRef, MatDialog } from '@angular/material';
import { SpinnerComponent } from '../../../logado/spinner/spinner.component';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.css']
})
export class LoginDialogComponent implements OnInit {
  funcionarioLogin = this.fb.group({
    Clinica: ['', Validators.required],
    SenhaClinica: ['', Validators.required],
    Email: ['', Validators.required],
    Senha: ['', Validators.required]
  });

  adminLogin = this.fb.group({
    Email: ['', [Validators.required, Validators.email]],
    Senha: ['', Validators.required]
  });

  constructor(private fb: FormBuilder,
    private loginService: LoginService,
    private dialogRef: MatDialogRef<LoginDialogComponent>,
    private spinnerDialog: MatDialog) { }

  ngOnInit() {
  }

  logarFuncionario() {
    this.carregando();
    this.loginService.funcionarioLogin(this.funcionarioLogin.value)
      .subscribe(res => this.dialogRef.close(true), error => this.dialogRef.close(false));
  }

  logarAdmin() {
    this.carregando();
    this.loginService.adminLogin(this.adminLogin.value)
      .subscribe(res => this.dialogRef.close(true), error => this.dialogRef.close(false));
  }

  carregando() {
    const spinner: MatDialogRef<SpinnerComponent> = this.spinnerDialog.open(SpinnerComponent, {
      panelClass: 'transparent',
      disableClose: true
    });
  }
}
