import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogConfig, MatSnackBar, MatDialogRef } from '@angular/material';
import { LoginDialogComponent } from './login-dialog/login-dialog.component';
import { SpinnerComponent } from '../../logado/spinner/spinner.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @Output() respostaLogin = new EventEmitter();

  constructor(private loginDialog: MatDialog,
    public alerta: MatSnackBar,
    private spinnerDialog: MatDialog) { }

  ngOnInit() {
  }

  openLoginDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;

    const loginDialogRef = this.loginDialog.open(LoginDialogComponent, dialogConfig);

    loginDialogRef.afterClosed().subscribe(res => {
      if (res === false) {
        this.alerta.open('Falha ao realizar Login!', '', {
          duration: 5000,
          panelClass: 'error-snackbar'
        });
        this.respostaLogin.emit(false);
      } else if (res === true) {
        this.alerta.open('Login realizado com sucesso!', '', {
          duration: 5000,
          panelClass: 'success-snackbar'
        });
        this.respostaLogin.emit(true);
      }
    });
  }

}
