import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  transform(clientes: any, termo: any): any {

    if (termo === undefined) {
      return clientes;
      }

    return clientes.filter(function(cliente) {
      return cliente.nome.toLowerCase().includes(termo.toLowerCase());
    });
  }

}
