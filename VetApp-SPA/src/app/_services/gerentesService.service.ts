import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GerentesServiceService {
  baseUrl = environment.apiUrl + 'funcionarios/';

  constructor(private http: HttpClient) { }

  cadastrarFuncionario(func) {
    return this.http.post(this.baseUrl + 'cadastrarFuncionario', func);
  }

}
