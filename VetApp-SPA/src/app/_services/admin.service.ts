import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { KeyValuePair } from '../_models/KeyValuePair';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  baseUrl = environment.apiUrl + 'admin/';

  constructor(private http: HttpClient) { }

  cadastrarClinica(clinica) {
    return this.http.post(this.baseUrl + 'cadastrarClinica', clinica);
  }

  cadastrarVacina(novaVacina: KeyValuePair) {
    return this.http.post(this.baseUrl + 'cadastrarVacina', novaVacina);
  }

}
