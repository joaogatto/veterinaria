import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { TokenModel } from '../_models/TokenModel';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  baseUrl = environment.apiUrl;
  jwtHelper = new JwtHelperService;

  tokenDecript: TokenModel;

  constructor(private http: HttpClient) { }

  adminLogin(admin: any) {
    return this.http.post(this.baseUrl + 'admin/login', admin).pipe(
        map((response: any) => {
            const func = response;
            if (func) {
                localStorage.setItem('token', func.token);
                this.tokenDecript = this.jwtHelper.decodeToken(func.token);
            }
        })
    );
  }

  funcionarioLogin(funcionario: any) {
    return this.http.post(this.baseUrl + 'funcionarios/login', funcionario).pipe(
        map((response: any) => {
            const func = response;
            if (func) {
                localStorage.setItem('token', func.token);
                this.tokenDecript = this.jwtHelper.decodeToken(func.token);
            }
        })
    );
  }

  logado() {
    const token = localStorage.getItem('token');

    if (!token) {
        return false;
    }

    if (this.jwtHelper.isTokenExpired(token)) {
        localStorage.removeItem('token');
        return false;
    }

    this.tokenDecript = this.jwtHelper.decodeToken(token);
    return true;
  }

  deslogar() {
      this.tokenDecript = null;
      localStorage.removeItem('token');

      if (this.logado()) {
          return false;
      }

      return true;
  }

}
