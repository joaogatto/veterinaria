/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GerentesServiceService } from './gerentesService.service';

describe('Service: GerentesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GerentesServiceService]
    });
  });

  it('should ...', inject([GerentesServiceService], (service: GerentesServiceService) => {
    expect(service).toBeTruthy();
  }));
});
