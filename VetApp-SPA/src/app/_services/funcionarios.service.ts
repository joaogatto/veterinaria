import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ClienteLista } from '../_models/ClienteLista';
import { Observable } from 'rxjs';
import { Especie } from '../_models/Especie';
import { Agendamento } from '../_models/Agendamento';
import { FuncionarioToAgendamento } from '../_models/FuncionarioToAgendamento';
import { AgendamentoToConsulta } from '../_models/AgendamentoToConsulta';
import { KeyValuePair } from '../_models/KeyValuePair';
import { VacinaReturn } from '../_models/VacinaReturn';
import { CancelarAgendamento } from '../_models/CancelarAgendamento';
import { ExameFinalizar } from '../_models/ExameFinalizar';
import { VacinaToVinculo } from '../_models/VacinaToVinculo';

@Injectable({
  providedIn: 'root'
})
export class FuncionariosService {
  baseUrl = environment.apiUrl + 'funcionarios/';
  animaisUrl = environment.apiUrl + 'animais/';
  agendamentosUrl = environment.apiUrl + 'agendamentos/';
  consultasUrl = environment.apiUrl + 'consultas/';
  vacinasUrl = environment.apiUrl + 'vacinas/';
  examesUrl = environment.apiUrl + 'exames/';

  constructor(private http: HttpClient) { }

  getClientes(): Observable<ClienteLista[]> {
    return this.http.get<ClienteLista[]>(this.baseUrl + 'getClientes');
  }

  cadastrarCliente(novoCliente) {
    return this.http.post(this.baseUrl + 'cadastrarCliente', novoCliente);
  }

  vincularCliente(cliente) {
    return this.http.post(this.baseUrl + 'vincularCliente', cliente);
  }

  editarCliente(cliente) {
    return this.http.put(this.baseUrl + 'atualizarCliente', cliente);
  }

  getCliente(clienteId: number): Observable<ClienteLista> {
    return this.http.get<ClienteLista>(this.baseUrl + 'getCliente/' + clienteId);
  }

  getEspecies(): Observable<Especie[]> {
    return this.http.get<Especie[]>(this.animaisUrl + 'getEspecies');
  }

  cadastrarAnimal(animal) {
    return this.http.post(this.animaisUrl + 'cadastrarAnimal', animal);
  }

  getAgendamentos(): Observable<Agendamento[]> {
    return this.http.get<Agendamento[]>(this.agendamentosUrl + 'getAgendamentos');
  }

  getFuncionariosToAgendamento(): Observable<FuncionarioToAgendamento[]> {
    return this.http.get<FuncionarioToAgendamento[]>(this.baseUrl + 'getFuncionariosToAgendamento');
  }

  getAgendamentosPorData(data: number): Observable<Agendamento[]> {
    return this.http.get<Agendamento[]>(this.agendamentosUrl + 'getAgendamentosPorData/' + data);
  }

  getAgendamentosFunc(): Observable<Agendamento[]> {
    return this.http.get<Agendamento[]>(this.agendamentosUrl + 'getAgendamentosFunc');
  }

  cadastrarAgendamento(agendamento) {
    return this.http.post(this.agendamentosUrl + 'cadastrarAgendamento', agendamento);
  }

  atualizarAgendamento(id, agendamento) {
    return this.http.put(this.agendamentosUrl + 'atualizarAgendamento/' + id, agendamento);
  }

  getAgendamentoToConsulta(agendamentoId): Observable<AgendamentoToConsulta> {
    return this.http.get<AgendamentoToConsulta>(this.agendamentosUrl + 'getAgendamentoConsulta/' + agendamentoId);
  }

  cadastrarConsulta(consulta) {
    return this.http.post(this.consultasUrl + 'cadastrarConsulta', consulta);
  }

  getAnimalToConsulta(animalId): Observable<AgendamentoToConsulta> {
    return this.http.get<AgendamentoToConsulta>(this.consultasUrl + 'getAnimalToConsulta/' + animalId);
  }

  getVacinas(): Observable<KeyValuePair[]> {
    return this.http.get<KeyValuePair[]>(this.vacinasUrl + 'getVacinas');
  }

  aplicarVacina(vacina): Observable<VacinaReturn> {
    return this.http.post<VacinaReturn>(this.vacinasUrl + 'aplicarVacina', vacina);
  }

  cancelarAgendamento(cancelamento: CancelarAgendamento) {
    return this.http.post(this.agendamentosUrl + 'cancelarAgendamento', cancelamento);
  }

  finalizarAgendamento(agendamentoId: number, foto: File ) {
    const formData: FormData = new FormData();
    formData.append('fotoArquivo', foto);

    return this.http.put(this.agendamentosUrl + 'finalizarAgendamento/' + agendamentoId, formData);
  }

  cadastrarExame(exame): Observable<KeyValuePair> {
    return this.http.post<KeyValuePair>(this.examesUrl + 'cadastrarExame', exame);
  }

  vincularArquivoExame(exameId: number, arquivo: File) {
    const formData: FormData = new FormData();
    formData.append('arquivo', arquivo);

    return this.http.put(this.examesUrl + 'vincularArquivoExame/' + exameId, formData);
  }

  getVacInjSemConsulta(animalId: number): Observable<VacinaToVinculo[]> {
    return this.http.get<VacinaToVinculo[]>(this.vacinasUrl + 'getVacinasSemConsulta/' + animalId);
  }
}
