import { Component, OnInit } from '@angular/core';
import { LoginService } from './_services/login.service';
import { Router } from '@angular/router';
import { MatDialogRef, MatDialog } from '@angular/material';
import { SpinnerComponent } from './logado/spinner/spinner.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  logado = false;
  usuario: any;


  ngOnInit() {
    this.logado = this.loginService.logado();
    console.log('logado = ' + this.logado);

    this.respostaDialog(this.logado);
  }

  constructor(private loginService: LoginService,
    private router: Router,
    private dialog: MatDialog) {
    if (this.loginService.logado()) {
      this.respostaDialog(true);
      this.logado = true;
    } else {
      this.logado = false;
    }
  }

  respostaDialog(resposta) {
    this.logado = resposta;
    if (this.logado === true) {
      const token = this.loginService.tokenDecript;
      if (token.role === 'Admin') {
        console.log('tokenrole = admin');
        this.router.navigate(['/admin/home']);
      } else if (token.role === 'Gerente' || token.role === 'Funcionario' || token.role === 'Veterinario') {
        console.log('appCrd');
        this.router.navigate(['/funcionarios/home']);
      }
    }
    this.dialog.closeAll();
  }

  respostaDeslogou(resposta) {
    this.logado = resposta;
  }

}
