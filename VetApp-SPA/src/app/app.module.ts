import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import ptBr from '@angular/common/locales/pt';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { MaterialModule } from './material';
import { MAT_DATE_LOCALE } from '@angular/material';

import { RouterModule } from '@angular/router';
import { appRoute } from './routes';

import { JwtModule } from '@auth0/angular-jwt';

import { FiltroPipe } from './_pipes/filtro.pipe';
import { DatePipe, registerLocaleData } from '@angular/common';

import { LoginService } from './_services/login.service';
import { FuncionariosService } from './_services/funcionarios.service';

import { LoginComponent } from './deslogado/login/login.component';
import { SideNavComponent } from './logado/side-nav/side-nav.component';
import { LoginDialogComponent } from './deslogado/login/login-dialog/login-dialog.component';
import { HomeAdminComponent } from './admin/home-admin/home-admin.component';
import { CadastrarClinicaComponent } from './admin/cadastrar-clinica/cadastrar-clinica.component';
import { HomeFuncionariosComponent } from './logado/funcionarios/home-funcionarios/home-funcionarios.component';
import { SpinnerComponent } from './logado/spinner/spinner.component';
import { ListaClientesComponent } from './logado/funcionarios/clientes/lista-clientes/lista-clientes.component';
import { TabsClientesComponent } from './logado/funcionarios/clientes/tabs-clientes/tabs-clientes.component';
import { CadastrarClienteComponent } from './logado/funcionarios/clientes/cadastrar-cliente/cadastrar-cliente.component';
import { VincularClienteComponent } from './logado/funcionarios/clientes/vincular-cliente/vincular-cliente.component';
import { PerfilClienteComponent } from './logado/funcionarios/clientes/perfil-cliente/perfil-cliente.component';
import { EditarClienteComponent } from './logado/funcionarios/clientes/editar-cliente/editar-cliente.component';
import { CadastrarAnimalComponent } from './logado/funcionarios/animais/cadastrar-animal/cadastrar-animal.component';
import { SelecionaClienteComponent } from './logado/funcionarios/dialogs/seleciona-cliente/seleciona-cliente.component';
import { CadastrarFuncionarioComponent } from './logado/gerentes/cadastrar-funcionario/cadastrar-funcionario.component';
import { GerentesServiceService } from './_services/gerentesService.service';
import { SelecionaAnimalComponent } from './logado/funcionarios/dialogs/seleciona-animal/seleciona-animal.component';
// tslint:disable-next-line:max-line-length
import { ListaAgendamentosClinicaComponent } from './logado/funcionarios/dialogs/lista-agendamentos-clinica/lista-agendamentos-clinica.component';
import { CadastrarAgendamentoComponent } from './logado/funcionarios/dialogs/cadastrar-agendamento/cadastrar-agendamento.component';
import { ListaAgendamentosComponent } from './logado/funcionarios/agendamentos/lista-agendamentos/lista-agendamentos.component';
import { TabsAgendamentosComponent } from './logado/funcionarios/agendamentos/tabs-agendamentos/tabs-agendamentos.component';
import { EditarAgendamentoComponent } from './logado/funcionarios/dialogs/editar-agendamento/editar-agendamento.component';
import { TabsConsultasComponent } from './logado/veterinarios/tabs-consultas/tabs-consultas.component';
import { MinhasConsultasComponent } from './logado/veterinarios/minhas-consultas/minhas-consultas.component';
import { ConsultaComponent } from './logado/veterinarios/consulta/consulta.component';
import { AddSintomaComponent } from './logado/veterinarios/vetDialogs/add-sintoma/add-sintoma.component';

import * as moment from 'moment';
import { AplicarVacinaComponent } from './logado/veterinarios/vetDialogs/aplicar-vacina/aplicar-vacina.component';
import { CadastrarVacinaComponent } from './admin/cadastrar-vacina/cadastrar-vacina.component';
import { CancelarAgendamentoComponent } from './logado/funcionarios/dialogs/cancelar-agendamento/cancelar-agendamento.component';
import { FinalizarAgendamentoComponent } from './logado/funcionarios/dialogs/finalizar-agendamento/finalizar-agendamento.component';
import { NovoExameComponent } from './logado/veterinarios/vetDialogs/novo-exame/novo-exame.component';
import { SelecionaTipoexameComponent } from './logado/veterinarios/vetDialogs/seleciona-tipoexame/seleciona-tipoexame.component';
import { AddMedicamentoComponent } from './logado/veterinarios/vetDialogs/add-medicamento/add-medicamento.component';
import { VincularVacinaEexameComponent } from './logado/veterinarios/vincular-vacina-eexame/vincular-vacina-eexame.component';
import { VincularVacInjExaComponent } from './logado/veterinarios/vetDialogs/vincular-vacInjExa/vincular-vacInjExa.component';
import { SelecionaConsultaComponent } from './logado/veterinarios/vetDialogs/seleciona-consulta/seleciona-consulta.component';
// tslint:disable-next-line:max-line-length
import { SelecionaVacinaInjecaoComponent } from './logado/veterinarios/vetDialogs/seleciona-vacinaInjecao/seleciona-vacinaInjecao.component';

export function tokenGetter() {
  return localStorage.getItem('token');
}

registerLocaleData(ptBr);

@NgModule({
  declarations: [
    FiltroPipe,
    AppComponent,
    SideNavComponent,
    LoginComponent,
    LoginDialogComponent,
    HomeAdminComponent,
    CadastrarClinicaComponent,
    HomeFuncionariosComponent,
    SpinnerComponent,
    ListaClientesComponent,
    TabsClientesComponent,
    CadastrarClienteComponent,
    VincularClienteComponent,
    PerfilClienteComponent,
    EditarClienteComponent,
    CadastrarAnimalComponent,
    SelecionaClienteComponent,
    SelecionaAnimalComponent,
    CadastrarFuncionarioComponent,
    ListaAgendamentosClinicaComponent,
    CadastrarAgendamentoComponent,
    ListaAgendamentosComponent,
    TabsAgendamentosComponent,
    EditarAgendamentoComponent,
    TabsConsultasComponent,
    MinhasConsultasComponent,
    ConsultaComponent,
    AddSintomaComponent,
    CadastrarVacinaComponent,
    AplicarVacinaComponent,
    CancelarAgendamentoComponent,
    FinalizarAgendamentoComponent,
    NovoExameComponent,
    SelecionaTipoexameComponent,
    AddMedicamentoComponent,
    VincularVacinaEexameComponent,
    SelecionaVacinaInjecaoComponent,
    SelecionaConsultaComponent,
    VincularVacInjExaComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoute),
    HttpClientModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['localhost:5000'],
        blacklistedRoutes: []
      }
    })
  ],
  providers: [
    LoginService,
    FuncionariosService,
    {
      provide: MAT_DATE_LOCALE,
      useValue: 'pt-br'
    },
    {
      provide: LOCALE_ID, useValue: 'pt'
    },
    DatePipe,
    GerentesServiceService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    SpinnerComponent,
    LoginDialogComponent,
    PerfilClienteComponent,
    EditarClienteComponent,
    CadastrarAnimalComponent,
    SelecionaClienteComponent,
    SelecionaAnimalComponent,
    ListaAgendamentosClinicaComponent,
    CadastrarAgendamentoComponent,
    EditarAgendamentoComponent,
    AddSintomaComponent,
    AplicarVacinaComponent,
    CancelarAgendamentoComponent,
    FinalizarAgendamentoComponent,
    NovoExameComponent,
    SelecionaTipoexameComponent,
    AddMedicamentoComponent,
    SelecionaVacinaInjecaoComponent,
    SelecionaConsultaComponent,
    VincularVacInjExaComponent
  ]
})
export class AppModule { }
